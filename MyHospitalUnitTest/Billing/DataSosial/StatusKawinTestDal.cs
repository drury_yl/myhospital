﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MyHospital.Billing.DataSosial.Dal;
using MyHospital.UnitTest.Helper;
using MyHospital.Billing.DataSosial;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class StatusKawinDalTest
    {
        private StatusKawinDal dalStatusKawin;

        public StatusKawinDalTest()
        {
            // Overwrite Connection string utk menghindari "kecelakaan" read-write database aktif
            dalStatusKawin = new StatusKawinDal();
            dalStatusKawin.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"DELETE    ta_status_kawin_dk";
                SqlCommand sqlCmd = new SqlCommand(sSql, conn);
                conn.Open();
                sqlCmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"DELETE    ta_status_kawin_dk";
                SqlCommand sqlCmd = new SqlCommand(sSql, conn);
                conn.Open();
                sqlCmd.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void Insert_00_NonExistedData_ExpectSucceed()
        {
            #region ARRANGE
            StatusKawin dataStatusKawin = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 1"
            };
            #endregion

            #region ACT
            dalStatusKawin.Insert(dataStatusKawin);
            #endregion

            #region ASSERT
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_ExistedData_ExpectSQLException()
        {
            #region ARRANGE
            StatusKawin dataStatusKawin1 = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 1"
            };
            StatusKawin dataStatusKawin2 = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 2"
            };
            #endregion

            #region ACT
            dalStatusKawin.Insert(dataStatusKawin1);
            dalStatusKawin.Insert(dataStatusKawin2);
            #endregion

            #region ASSERT
            #endregion
        }

        [TestMethod]
        public void Update_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            StatusKawin dataStatusKawin1 = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 1"
            };
            StatusKawin dataStatusKawin2 = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 2"
            };
            #endregion

            #region ACT
            dalStatusKawin.Insert(dataStatusKawin1);
            dalStatusKawin.Update(dataStatusKawin2);
            #endregion

            #region ASSERT
            #endregion
        }

        [TestMethod]
        public void Update_01_NonExistedData_ExpectSucceed()
        {
            #region ARRANGE
            StatusKawin dataStatusKawin = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 1"
            };
            #endregion

            #region ACT
            dalStatusKawin.Update(dataStatusKawin);
            #endregion

            #region ASSERT
            #endregion
        }

        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            StatusKawin dataStatusKawin = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 1"
            };
            dalStatusKawin.Insert(dataStatusKawin);
            #endregion

            #region ACT
            dalStatusKawin.Delete(dataStatusKawin);
            #endregion

            #region ASSERT
            #endregion
        }

        [TestMethod]
        public void Delete_01_NonExistedData_ExpectSucceed()
        {
            #region ARRANGE
            StatusKawin dataStatusKawin = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 1"
            };
            #endregion

            #region ACT
            dalStatusKawin.Delete(dataStatusKawin);
            #endregion

            #region ASSERT
            #endregion
        }

        [TestMethod]
        public void GetById_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            StatusKawin expected = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 1"
            };
            dalStatusKawin.Insert(expected);
            #endregion

            #region ACT
            StatusKawin actual = dalStatusKawin.GetById("A");
            #endregion

            #region ASSERT
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            #endregion
        }

        [TestMethod]
        public void ListAll_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            StatusKawin expected1 = new StatusKawin
            {
                Kode = "A",
                Nama = "Status Kawin 1"
            };
            StatusKawin expected2 = new StatusKawin
            {
                Kode = "B",
                Nama = "Status Kawin 2"
            };
            dalStatusKawin.Insert(expected1);

            dalStatusKawin.Insert(expected2);
            #endregion

            #region ACT
            List<StatusKawin> actual = dalStatusKawin.ListAll();
            #endregion

            #region ASSERT
            StatusKawin actual1 = actual.Find(x => x.Kode == expected1.Kode);
            StatusKawin actual2 = actual.Find(x => x.Kode == expected2.Kode);

            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);

            Assert.AreEqual(expected2.Kode, actual2.Kode);
            Assert.AreEqual(expected2.Nama, actual2.Nama);
            #endregion
        }
    }
}
