﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

using Ics.Helper7.BaseClass;

using MyHospital.Billing.DataSosial.Ci;
using MyHospital.Billing.DataSosial.Bl;
using MyHospital.Billing.DataSosial;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class GolonganDarahTestBl
    {
        private IGolonganDarahDal mockDalGolonganDarah;
        private IGolonganDarahBL blGolonganDarah;


        [TestInitialize]
        public void TestInit()
        {
            mockDalGolonganDarah = MockRepository.GenerateStub<IGolonganDarahDal>(); 
            blGolonganDarah = new GolonganDarahBl()
            {
                 DalGolonganDarah = mockDalGolonganDarah 
            };
        }
        [TestCleanup]
        public void TestCleanUp()
        {

        }

        [TestMethod]
        public void Save_00_ValidData_ExpectSucceed()
        {
            #region ARRANGE
            int expected = 0;

            // stub object yang tidak di-test
            GolonganDarah fakeGolonganDarah = null;
            mockDalGolonganDarah.Stub(x => x.GetById("A")).Return(fakeGolonganDarah);

            GolonganDarah dataGolonganDarah  = new GolonganDarah
            {
                Kode = "O",
                Nama = "O"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blGolonganDarah.Save(dataGolonganDarah);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Count);
            #endregion
        }

        [TestMethod]
        public void Save_01_KodeGolonganDarahKosong_ExpectFailed_BR01()
        {
            #region ARRANGE
            string expected = "BR-01";
            GolonganDarah fakeGolonganDarah = new GolonganDarah{ Kode = " ", Nama = " " };
            mockDalGolonganDarah.Stub(x => x.GetById("A")).Return(fakeGolonganDarah);
            //
            GolonganDarah dataGolonganDarah = new GolonganDarah
            {
                Kode = " ",
                Nama = "O"
            };
            #endregion


            #region ACT
            MethodRetVal actual = blGolonganDarah.Save(dataGolonganDarah);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_02_NamaGolonganDarah_ExpectFailed_BR02()
        {
            #region ARRANGE
            string expected = "BR-02";

            GolonganDarah fakeGolonganDarah = new GolonganDarah { Kode = " ", Nama = " " };
            mockDalGolonganDarah.Stub(x => x.GetById("A")).Return(fakeGolonganDarah);

            GolonganDarah dataGolonganDarah = new GolonganDarah
            {
                Kode = "O",
                Nama = " "
            };
            #endregion

            #region ACT
            MethodRetVal actual = blGolonganDarah.Save(dataGolonganDarah);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_03_NewDataCallInsert_ExpectSucceed()
        {
            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalGolonganDarah.Stub(x => x.GetById("O")).Return(null);

            GolonganDarah dataGolonganDarah = new GolonganDarah
            {
                Kode = "O",
                Nama = "O"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blGolonganDarah.Save(dataGolonganDarah);
            #endregion

            #region ASSERT
            mockDalGolonganDarah.AssertWasCalled(x => x.Insert((dataGolonganDarah)));
            mockDalGolonganDarah.AssertWasNotCalled(x => x.Update((dataGolonganDarah)));
            #endregion
        }

        [TestMethod]
        public void Save_04_EditDataCallUpdate_ExpectSucceed()
        {

            #region ARRANGE
            mockDalGolonganDarah.Stub(x => x.GetById("O")).Return(new GolonganDarah
            {
                Kode = "O",
                Nama = "O"
            });
            GolonganDarah dataGolonganDarah = new GolonganDarah
            {
                Kode = "O",
                Nama = "P"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blGolonganDarah.Save(dataGolonganDarah);
            #endregion

            #region ASSERT
            mockDalGolonganDarah.AssertWasCalled(x => x.Update((dataGolonganDarah)));
            mockDalGolonganDarah.AssertWasNotCalled(x => x.Insert((dataGolonganDarah)));
            #endregion
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            GolonganDarah dataGolonganDarah = new GolonganDarah
            {
                Kode = "O",
                Nama = "O"
            };
            mockDalGolonganDarah.Expect(x => x.Delete(dataGolonganDarah));
            #endregion

            #region ACT
            MethodRetVal retVal = blGolonganDarah.Delete(dataGolonganDarah);
            #endregion

            #region ASSERT
            mockDalGolonganDarah.VerifyAllExpectations();
            #endregion
        }


    }
}
