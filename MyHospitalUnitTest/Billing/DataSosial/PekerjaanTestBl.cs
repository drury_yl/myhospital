﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHospital.Billing.DataSosial;
using MyHospital.Billing.DataSosial.Bl;
using Rhino.Mocks;
using Ics.Helper7.BaseClass;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class PekerjaanTestBl
    {
        private PekerjaanBl blPekerjaan;
        private IPekerjaanDAL mockPekerjaanDal;

        [TestInitialize]
        public void TestInit()
        {
            mockPekerjaanDal = MockRepository.GenerateStub<IPekerjaanDAL>();
            blPekerjaan = new PekerjaanBl
            {
                DalPekerjaan = mockPekerjaanDal
            };
        }

        [TestCleanup]
        public void TestCleanUp()
        {

        }

        [TestMethod]
        public void Save_00_NewData_ExpectSucceed()
        {
            #region arrange
            mockPekerjaanDal.Stub(x => x.GetById("A")).Return(null);

            Pekerjaan dataPekerjaan = new Pekerjaan
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region act
            MethodRetVal actual = blPekerjaan.Save(dataPekerjaan);
            #endregion

            #region assert
            mockPekerjaanDal.AssertWasCalled(x => x.Insert(dataPekerjaan));
            mockPekerjaanDal.AssertWasNotCalled(x => x.Update(dataPekerjaan)); 
            #endregion
        }

        [TestMethod]
        public void Save_01_DataExist_ExpectSucceed()
        {
            #region arrange
            Pekerjaan dataPekerjaanExist = new Pekerjaan
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockPekerjaanDal.Stub(x => x.GetById("A")).Return(dataPekerjaanExist);

            Pekerjaan dataPekerjaan = new Pekerjaan
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region act
            MethodRetVal actual = blPekerjaan.Save(dataPekerjaan);
            #endregion

            #region assert
            mockPekerjaanDal.AssertWasCalled(x => x.Update(dataPekerjaan));
            mockPekerjaanDal.AssertWasNotCalled(x => x.Insert(dataPekerjaan)); 
            #endregion
        }

        [TestMethod]
        public void Save_02_KodeKosong_ExpectFailed_BR01()
        {
            #region arrange
            string expected = "BR-01";

            Pekerjaan dataPekerjaan = new Pekerjaan
            {
                Kode = "",
                Nama = "Data1"
            };
            #endregion

            #region act
            MethodRetVal actual = blPekerjaan.Save(dataPekerjaan);
            #endregion

            #region assert
            Assert.IsTrue(actual.Items.Exists(x => x.ErrId == expected)); 
            #endregion
        }

        [TestMethod]
        public void Save_03_KodeKosong_ExpectFailed_BR02()
        {
            #region arrange
            string expected = "BR-02";

            Pekerjaan dataPekerjaan = new Pekerjaan
            {
                Kode = "A",
                Nama = ""
            };
            #endregion

            #region act
            MethodRetVal actual = blPekerjaan.Save(dataPekerjaan);
            #endregion

            #region assert
            Assert.IsTrue(actual.Items.Exists(x => x.ErrId == expected)); 
            #endregion
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            #region arrange
            Pekerjaan dataPekerjaan = new Pekerjaan
            {
                Kode = "A",
                Nama = "Dtaa1"
            };
            #endregion

            #region act
            MethodRetVal actual = blPekerjaan.Delete(dataPekerjaan);
            #endregion

            #region assert
            mockPekerjaanDal.AssertWasCalled(x => x.Delete(dataPekerjaan)); 
            #endregion
        }

        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {
            #region arrange
            Pekerjaan dataPekerjaan = new Pekerjaan
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockPekerjaanDal.Expect(x => x.GetById("A")).Return(dataPekerjaan);
            #endregion

            #region act
            Pekerjaan actual = blPekerjaan.GetById("A");
            #endregion

            #region assert
            mockPekerjaanDal.VerifyAllExpectations(); 
            #endregion
        }
    }
}
