﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MyHospital.Billing.DataSosial.Dal;
using MyHospital.UnitTest.Helper;
using System.Data.SqlClient;
using MyHospital.Billing.DataSosial;
using System.Collections.Generic;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class AgamaTestDal
    {
        private AgamaDal dalAgama;


        public AgamaTestDal()
        {
            dalAgama = new AgamaDal();
            dalAgama.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"
                    DELETE      ta_agama ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"
                    DELETE      ta_agama ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }


        [TestMethod]
        public void Insert_00_NewData_ExpectSucceed()
        {
            // arrange
            Agama dataAgama = new Agama
            {
                Kode = "A",
                Nama = "Agama1"
            };

            // act
            dalAgama.Insert(dataAgama);


            // assert
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_DuplicateData_ExpectSqlException()
        {
            // arrange
            Agama dataAgama1 = new Agama
            {
                Kode = "A",
                Nama = "Agama1"
            };
            dalAgama.Insert(dataAgama1);
            Agama dataAgama2 = new Agama
            {
                Kode = "A",
                Nama = "Agama2"
            };

            // act
            dalAgama.Insert(dataAgama2);

            // assert
        }

        [TestMethod]
        public void Update_00_DataExist_ExpectSucceed()
        {
            // arrange
            Agama dataAgama1 = new Agama
            {
                Kode = "A",
                Nama = "Agama1"
            };
            dalAgama.Insert(dataAgama1);

            Agama dataAgama2 = new Agama
            {
                Kode = "A",
                Nama = "Agama2"
            };


            // act
            dalAgama.Update(dataAgama2);

            // assert
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            // arrange
            Agama dataAgama = new Agama
            {
                Kode = "A",
                Nama = "Agama1"
            };
            dalAgama.Insert(dataAgama);

            // act
            dalAgama.Delete(dataAgama);

            // assert
        }

        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {
            // arrange
            Agama expected = new Agama
            {
                Kode = "A",
                Nama = "Agama1"
            };
            dalAgama.Insert(expected);

            // act
            Agama actual = dalAgama.GetById("A");

            // assert
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
        }

        [TestMethod]
        public void ListAll_00_DataExist_ExpectSucceed()
        {
            // arrange
            Agama expected1 = new Agama
            {
                Kode = "A",
                Nama = "Agama1"
            };
            dalAgama.Insert(expected1);
            Agama expected2 = new Agama
            {
                Kode = "B",
                Nama = "Agama2"
            };
            dalAgama.Insert(expected2);

            // act
            List<Agama> actual = dalAgama.ListAll();

            // assert
            Agama actual1 = actual.Find(x => x.Kode == "A");

            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
        }
    }
}
