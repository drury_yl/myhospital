﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Rhino.Mocks;

using Ics.Helper7.BaseClass;

using MyHospital.Billing.DataSosial.Ci;
using MyHospital.Billing.DataSosial.Bl;
using MyHospital.Billing.DataSosial;

namespace MyHospital.UnitTest.Billing.DataSosial
{

    [TestClass]
    public class KabupatenTestBl
    {

        private IKabupatenDal mockDalKabupaten;
        private IPropinsiDal mockDalPropinsi;
        private KabupatenBl blKabupaten;


        [TestInitialize]
        public void TestInit()
        {
            mockDalKabupaten = MockRepository.GenerateStub<IKabupatenDal>();
            mockDalPropinsi = MockRepository.GenerateStub<IPropinsiDal>();
            blKabupaten = new KabupatenBl()
            {
                DalKabupaten = mockDalKabupaten,
                DalPropinsi = mockDalPropinsi
            };
        }

        [TestCleanup]
        public void TestCleanUp()
        {

        }

        [TestMethod]
        public void Save_00_ValidData_ExpectSucceed()
        {
            #region ARRANGE
            //  expected no error :-D
            int expected = 0;

            // stub object yang tidak di-test
            Kabupaten fakeKabupaten = null;
            mockDalKabupaten.Stub(x => x.GetById("A")).Return(fakeKabupaten);

            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blKabupaten.Save(dataKabupaten);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Count);
            #endregion
        }

        [TestMethod]
        public void Save_01_KodeKabupatenKosong_ExpectFailed_BR01()
        {
            #region ARRANGE
            // set expected value
            string expected = "BR-01";
            //  stub object yang tidak di-test
            Kabupaten fakeKabupaten = new Kabupaten { Kode = " ", Nama = " " };
            mockDalKabupaten.Stub(x => x.GetById("A")).Return(fakeKabupaten);
            //
            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = " ",
                Nama = "Data1"
            };
            #endregion


            #region ACT
            MethodRetVal actual = blKabupaten.Save(dataKabupaten);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_02_NamaKabupatenKosong_ExpectFailed_BR02()
        {
            #region ARRANGE
            // set expected value
            string expected = "BR-02";

            //  stub object yang tidak di-test
            Kabupaten fakeKabupaten = new Kabupaten { Kode = " ", Nama = " " };
            mockDalKabupaten.Stub(x => x.GetById("A")).Return(fakeKabupaten);

            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = "A",
                Nama = " "
            };
            #endregion

            #region ACT
            MethodRetVal actual = blKabupaten.Save(dataKabupaten);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_03_NewDataCallInsert_ExpectSucceed()
        {
            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalKabupaten.Stub(x => x.GetById("A")).Return(null);

            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blKabupaten.Save(dataKabupaten);
            #endregion

            #region ASSERT
            mockDalKabupaten.AssertWasCalled(x => x.Insert((dataKabupaten)));
            mockDalKabupaten.AssertWasNotCalled(x => x.Update((dataKabupaten)));
            #endregion
        }

        [TestMethod]
        public void Save_04_EditDataCallUpdate_ExpectSucceed()
        {

            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalKabupaten.Stub(x => x.GetById("A")).Return(new Kabupaten
            {
                Kode = "A",
                Nama = "Data1"
            });
            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blKabupaten.Save(dataKabupaten);
            #endregion

            #region ASSERT
            mockDalKabupaten.AssertWasCalled(x => x.Update((dataKabupaten)));
            mockDalKabupaten.AssertWasNotCalled(x => x.Insert((dataKabupaten)));
            #endregion
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockDalKabupaten.Expect(x => x.Delete(dataKabupaten));
            #endregion

            #region ACT
            MethodRetVal retVal = blKabupaten.Delete(dataKabupaten);
            #endregion

            #region ASSERT
            mockDalKabupaten.VerifyAllExpectations();
            #endregion
        }

        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            Kabupaten expected = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockDalKabupaten.Expect(x => x.GetById("A")).Return(expected);
            #endregion

            #region ACT
            Kabupaten actual = blKabupaten.GetById("A");
            #endregion

            #region ASSERT
            mockDalKabupaten.VerifyAllExpectations();
            #endregion
        }

        [TestMethod]
        public void LIstAll_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            List<Kabupaten> expected = new List<Kabupaten>
            {
                new Kabupaten { Kode = "A", Nama = "Data1" },
                new Kabupaten { Kode = "B", Nama = "Data2" }
            };
            mockDalKabupaten.Expect(x => x.ListAll()).Return(expected);
            #endregion

            #region ACT
            List<Kabupaten> actual = blKabupaten.ListAll();
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual);
            #endregion
        }
    }
}
