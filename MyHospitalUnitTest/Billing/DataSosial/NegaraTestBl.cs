﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHospital.Billing.DataSosial.Ci;
using MyHospital.Billing.DataSosial.Bl;
using Rhino.Mocks;
using MyHospital.Billing.DataSosial;
using Ics.Helper7.BaseClass;
using System.Collections.Generic;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class NegaraTestBl
    {

        private INegaraDal mockDalNegara;
        private NegaraBl blNegara;


        [TestInitialize]
        public void TestInit()
        {
            mockDalNegara = MockRepository.GenerateStub<INegaraDal>();
            blNegara = new NegaraBl()
            {
                DalNegara = mockDalNegara
            };
        }

        [TestCleanup]
        public void TestCleanUp()
        {

        }

        [TestMethod]
        public void Save_00_ValidData_ExpectSucceed()
        {
            #region ARRANGE
            //  expected no error :-D
            int expected = 0;

            // stub object yang tidak di-test
            Negara fakeNegara = null;
            mockDalNegara.Stub(x => x.GetById("A")).Return(fakeNegara);

            Negara dataNegara = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blNegara.Save(dataNegara);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Count);
            #endregion
        }

        [TestMethod]
        public void Save_01_KodeNegaraKosong_ExpectFailed_BR01()
        {
            #region ARRANGE
            // set expected value
            string expected = "BR-01";
            //  stub object yang tidak di-test
            Negara fakeNegara = new Negara { Kode = " ", Nama = " " };
            mockDalNegara.Stub(x => x.GetById("A")).Return(fakeNegara);
            //
            Negara dataNegara = new Negara
            {
                Kode = " ",
                Nama = "Negara1"
            };
            #endregion


            #region ACT
            MethodRetVal actual = blNegara.Save(dataNegara);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_02_NamaNegaraKosong_ExpectFailed_BR02()
        {
            #region ARRANGE
            // set expected value
            string expected = "BR-02";

            //  stub object yang tidak di-test
            Negara fakeNegara = new Negara { Kode = " ", Nama = " " };
            mockDalNegara.Stub(x => x.GetById("A")).Return(fakeNegara);

            Negara dataNegara = new Negara
            {
                Kode = "A",
                Nama = " "
            };
            #endregion

            #region ACT
            MethodRetVal actual = blNegara.Save(dataNegara);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_03_NewDataCallInsert_ExpectSucceed()
        {
            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalNegara.Stub(x => x.GetById("A")).Return(null);

            Negara dataNegara = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blNegara.Save(dataNegara);
            #endregion

            #region ASSERT
            mockDalNegara.AssertWasCalled(x => x.Insert((dataNegara)));
            mockDalNegara.AssertWasNotCalled(x => x.Update((dataNegara)));
            #endregion
        }

        [TestMethod]
        public void Save_04_EditDataCallUpdate_ExpectSucceed()
        {

            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalNegara.Stub(x => x.GetById("A")).Return(new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            });
            Negara dataNegara = new Negara
            {
                Kode = "A",
                Nama = "Negara2"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blNegara.Save(dataNegara);
            #endregion

            #region ASSERT
            mockDalNegara.AssertWasCalled(x => x.Update((dataNegara)));
            mockDalNegara.AssertWasNotCalled(x => x.Insert((dataNegara)));
            #endregion
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            Negara dataNegara = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            mockDalNegara.Expect(x => x.Delete(dataNegara));
            #endregion

            #region ACT
            MethodRetVal retVal = blNegara.Delete(dataNegara);
            #endregion

            #region ASSERT
            mockDalNegara.VerifyAllExpectations();
            #endregion
        }

        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            Negara expected = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            mockDalNegara.Expect(x => x.GetById("A")).Return(expected);
            #endregion

            #region ACT
            Negara actual = blNegara.GetById("A");
            #endregion

            #region ASSERT
            mockDalNegara.VerifyAllExpectations();
            #endregion
        }

        [TestMethod]
        public void LIstAll_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            List<Negara> expected = new List<Negara>
            {
                new Negara { Kode = "A", Nama = "Negara1" },
                new Negara { Kode = "B", Nama = "Negara2" }
            };
            mockDalNegara.Expect(x => x.ListAll()).Return(expected);
            #endregion

            #region ACT
            List<Negara> actual = blNegara.ListAll();
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual);
            #endregion
        }
    }
}
