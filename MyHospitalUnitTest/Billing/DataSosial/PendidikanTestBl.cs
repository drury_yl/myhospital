﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Rhino.Mocks;
using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial.Ci;
using MyHospital.Billing.DataSosial.Bl;
using MyHospital.Billing.DataSosial;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class PendidikanTestBl
    {
        private IPendidikanDal mockDalPendidikan;
        private PendidikanBl blPendidikan;

        [TestInitialize]
        public void TestInit()
        {
            mockDalPendidikan = MockRepository.GenerateStub<IPendidikanDal>();
            blPendidikan = new PendidikanBl()
            {
                DalPendidikan = mockDalPendidikan
            };
        }

        [TestCleanup]
        public void TestCleanUp()
        {

        }

        [TestMethod]
        public void Save_00_ValidData_ExpectSucceed()
        {
            #region
            int expected = 0;

            //stub object
            Pendidikan fakePendidikan = null;
            mockDalPendidikan.Stub(x => x.GetById("A")).Return(fakePendidikan);

            Pendidikan dataPendidikan = new Pendidikan
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region
            MethodRetVal actual = blPendidikan.Save(dataPendidikan);
            #endregion

            #region
            Assert.AreEqual(expected, actual.Items.Count);
            #endregion

        }

        [TestMethod]
        public void Save_01_kodePendidikanKosong_ExpectFailed()
        {
            #region
            //set expected value
            string expected = "BR-01";

            //stub object
            Pendidikan fakePendidikan = new Pendidikan { Kode=" ", Nama=" "};
            mockDalPendidikan.Stub(x => x.GetById("A")).Return(fakePendidikan);

            Pendidikan dataPendidikan = new Pendidikan
            {
                Kode = " ",
                Nama = "Data1"
            };
            #endregion

            #region
            MethodRetVal actual = blPendidikan.Save(dataPendidikan);
            #endregion

            #region
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion

        }

        [TestMethod]
        public void Save_02_NamaPendidikanKosong_ExpectFailed()
        {
            #region
            string expected = "BR-02";

            //stub object
            Pendidikan fakePendidikan = new Pendidikan { Kode=" ", Nama= " "};
            mockDalPendidikan.Stub(x => x.GetById("A")).Return(fakePendidikan);

            Pendidikan dataPendidikan = new Pendidikan
            {
                Kode = "A",
                Nama = " "
            };
            #endregion

            #region
            MethodRetVal actual = blPendidikan.Save(dataPendidikan);
            #endregion

            #region
            Assert.AreEqual(expected, actual.Items.Find(x=> x.ErrId == expected).ErrId);
            #endregion

        }

    }
}
