﻿using System;

using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial;
using MyHospital.UnitTest.Helper;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class GolonganDarahDalTest
    {
        private GolonganDarahDal dalGolonganDarah;
        public GolonganDarahDalTest()
        {
            dalGolonganDarah = new GolonganDarahDal();
            dalGolonganDarah.OverwriteConnectionString(DbTestHelper.GetTestDbConnString()); 
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_gol_darah";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"DELETE ta_gol_darah";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void Insert_00_NonExistData_ExspectSucces ()
        {
            #region ARRANGE
            GolonganDarah dataGolonganDarah = new GolonganDarah
            {
                Kode = "O",
                Nama = "O"
            };
            #endregion

            #region ACT
            dalGolonganDarah.Insert(dataGolonganDarah);
            #endregion

            #region ASSERT 

            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_ExistedData_ExspectSqlException()
        {
            #region ARRANGE
            GolonganDarah dataGolonganDarah = new GolonganDarah
            {
                Kode = "O",
                Nama = "O"
            };
            dalGolonganDarah.Insert(dataGolonganDarah);
            GolonganDarah dataGolonganDarah2 = new GolonganDarah()
            {
                Kode = "O", 
                Nama = "P"
            };
            #endregion

            #region ACT 
            dalGolonganDarah.Insert(dataGolonganDarah2); 
            #endregion

            #region ASSERT 

            #endregion

        }

        [TestMethod]
        public void Update_00_Existed_data_ExspectSucces()
        {
            #region ARRANGE 
            GolonganDarah dataGolonganDarah = new GolonganDarah()
            {
                Kode = "O",
                Nama = "O"
            };
            dalGolonganDarah.Insert(dataGolonganDarah);

            GolonganDarah dataGolonganDarah2 = new GolonganDarah()
            {
                Kode = "O",
                Nama = "P"

            };
            #endregion
            #region ACT 
            dalGolonganDarah.Update(dataGolonganDarah2);
            #endregion
        }
        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {

            #region ARRANGE
            GolonganDarah dataGolonganDarah = new GolonganDarah
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalGolonganDarah.Insert(dataGolonganDarah);
            #endregion

            #region ACT
            dalGolonganDarah.Delete(dataGolonganDarah);
            #endregion

            #region ASSERT
            #endregion
        }

        [TestMethod]
        public void delete_01_nonExistedData_ExspectSucces()
        {
            #region ARRANGE 
            GolonganDarah dataGolonganDarah = new GolonganDarah()
            {
                Kode = "O",
                Nama = "O"
            };
            #endregion
            #region ACT
            dalGolonganDarah.Delete(dataGolonganDarah); 
            #endregion
        }

        [TestMethod]
        public void GetById_00_ExistedData_ExspectSucces()
        {
            #region ARRANGE 
            GolonganDarah expected = new GolonganDarah()
            {
                Kode = "O",
                Nama = "O"
            };
            #endregion
            
            #region ACT 
            GolonganDarah actual = dalGolonganDarah.GetById("O");
            #endregion

            #region ASSERT 
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            #endregion
        }
        [TestMethod]
        public void ListAll_00_ExistedData_ExspectSucces()
        {
            #region ARRANGE 
            GolonganDarah expected1 = new GolonganDarah()
            {
                Kode = "O",
                Nama = "O"
            };
            GolonganDarah expected2 = new GolonganDarah
            {
                Kode = "B",
                Nama = "B"
            };
            dalGolonganDarah.Insert(expected1);
            dalGolonganDarah.Insert(expected2);
            #endregion

            #region ACT
            List<GolonganDarah> actual = dalGolonganDarah.ListAll();
            #endregion

            #region ASSERT 
            GolonganDarah actual1 = actual.Find(x => x.Kode == expected1.Kode);
            GolonganDarah actual2 = actual.Find(x => x.Kode == expected2.Kode);

            #endregion

            #region ASSERT 
            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
            Assert.AreEqual(expected2.Kode, actual2.Kode);
            Assert.AreEqual(expected2.Nama, actual2.Nama);


            #endregion
        }


    }
}
