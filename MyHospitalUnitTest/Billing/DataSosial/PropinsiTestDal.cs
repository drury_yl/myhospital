﻿using System;

using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial;
using MyHospital.UnitTest.Helper;


namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class PropinsiDalTests
    {
        private PropinsiDal dalPropinsi;

        public PropinsiDalTests()
        {
            // Overwrite Connection string utk menghindari "kecelakaan" read-write database aktif
            dalPropinsi = new PropinsiDal();
            dalPropinsi.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_propinsi";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_propinsi";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }


        [TestMethod]
        public void Insert_00_NonExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Propinsi dataPropinsi = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            dalPropinsi.Insert(dataPropinsi);
            #endregion

            #region ASSERT
            // expect succed on void procedure, no assert needed
            #endregion

        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_ExistedData_ExpectSqlException()
        {
            #region ARRANGE
            Propinsi dataPropinsi1 = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalPropinsi.Insert(dataPropinsi1);
            Propinsi dataPropinsi2 = new Propinsi
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            dalPropinsi.Insert(dataPropinsi2);
            #endregion

            #region ASSERT
            // Expect Exception, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Propinsi dataPropinsi1 = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalPropinsi.Insert(dataPropinsi1);
            Propinsi dataPropinsi2 = new Propinsi
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            dalPropinsi.Update(dataPropinsi2);
            #endregion

            #region VERIFY
            // expect succed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_01_NonExistedData_ExpectSucceed()
        {
            #region INIT
            Propinsi dataPropinsi1 = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region CALLING
            dalPropinsi.Update(dataPropinsi1);
            #endregion

            #region VERIFY
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {

            #region ARRANGE
            Propinsi dataPropinsi = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalPropinsi.Insert(dataPropinsi);
            #endregion

            #region ACT
            dalPropinsi.Delete(dataPropinsi);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_01_NonExistedData_ExpectSucceed()
        {

            #region ARRANGE
            Propinsi dataPropinsi = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            dalPropinsi.Delete(dataPropinsi);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void GetById_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Propinsi expected = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalPropinsi.Insert(expected);
            #endregion

            #region ACT
            Propinsi actual = dalPropinsi.GetById("A");
            #endregion

            #region ASSERT
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            #endregion
        }

        [TestMethod]
        public void ListAll_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Propinsi expected1 = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            Propinsi expected2 = new Propinsi
            {
                Kode = "B",
                Nama = "Data2"
            };
            dalPropinsi.Insert(expected1);
            dalPropinsi.Insert(expected2);
            #endregion

            #region ACT
            List<Propinsi> actual = dalPropinsi.ListAll();
            #endregion

            #region ASSERT
            Propinsi actual1 = actual.Find(x => x.Kode == expected1.Kode);
            Propinsi actual2 = actual.Find(x => x.Kode == expected2.Kode);
            //
            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
            Assert.AreEqual(expected2.Kode, actual2.Kode);
            Assert.AreEqual(expected2.Nama, actual2.Nama);
            #endregion
        }
    }
}
