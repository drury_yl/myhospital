﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Rhino.Mocks;

using MyHospital.Billing.DataSosial.Ci;
using MyHospital.Billing.DataSosial.Bl;
using MyHospital.Billing.DataSosial;
using Ics.Helper7.BaseClass;

namespace MyHospital.UnitTest.Billing.DataSosial
{

    [TestClass]
    public class SukuTestBl
    {

        private ISukuDal mockDalSuku;
        private SukuBl blSuku;


        [TestInitialize]
        public void TestInit()
        {
            mockDalSuku = MockRepository.GenerateStub<ISukuDal>();
            blSuku = new SukuBl()
            {
                DalSuku = mockDalSuku
            };
        }

        [TestCleanup]
        public void TestCleanUp()
        {

        }

        [TestMethod]
        public void Save_00_ValidData_ExpectSucceed()
        {
            #region ARRANGE
            //  expected no error :-D
            int expected = 0;

            // stub object yang tidak di-test
            Suku fakeSuku = null;
            mockDalSuku.Stub(x => x.GetById("A")).Return(fakeSuku);

            Suku dataSuku = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blSuku.Save(dataSuku);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Count);
            #endregion
        }

        [TestMethod]
        public void Save_01_KodeSukuKosong_ExpectFailed_BR01()
        {
            #region ARRANGE
            // set expected value
            string expected = "BR-01";
            //  stub object yang tidak di-test
            Suku fakeSuku = new Suku { Kode = " ", Nama = " " };
            mockDalSuku.Stub(x => x.GetById("A")).Return(fakeSuku);
            //
            Suku dataSuku = new Suku
            {
                Kode = " ",
                Nama = "Data1"
            };
            #endregion


            #region ACT
            MethodRetVal actual = blSuku.Save(dataSuku);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_02_NamaSukuKosong_ExpectFailed_BR02()
        {
            #region ARRANGE
            // set expected value
            string expected = "BR-02";

            //  stub object yang tidak di-test
            Suku fakeSuku = new Suku { Kode = " ", Nama = " " };
            mockDalSuku.Stub(x => x.GetById("A")).Return(fakeSuku);

            Suku dataSuku = new Suku
            {
                Kode = "A",
                Nama = " "
            };
            #endregion

            #region ACT
            MethodRetVal actual = blSuku.Save(dataSuku);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_03_NewDataCallInsert_ExpectSucceed()
        {
            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalSuku.Stub(x => x.GetById("A")).Return(null);

            Suku dataSuku = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blSuku.Save(dataSuku);
            #endregion

            #region ASSERT
            mockDalSuku.AssertWasCalled(x => x.Insert((dataSuku)));
            mockDalSuku.AssertWasNotCalled(x => x.Update((dataSuku)));
            #endregion
        }

        [TestMethod]
        public void Save_04_EditDataCallUpdate_ExpectSucceed()
        {

            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalSuku.Stub(x => x.GetById("A")).Return(new Suku
            {
                Kode = "A",
                Nama = "Data1"
            });
            Suku dataSuku = new Suku
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blSuku.Save(dataSuku);
            #endregion

            #region ASSERT
            mockDalSuku.AssertWasCalled(x => x.Update((dataSuku)));
            mockDalSuku.AssertWasNotCalled(x => x.Insert((dataSuku)));
            #endregion
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            Suku dataSuku = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockDalSuku.Expect(x => x.Delete(dataSuku));
            #endregion

            #region ACT
            MethodRetVal retVal = blSuku.Delete(dataSuku);
            #endregion

            #region ASSERT
            mockDalSuku.VerifyAllExpectations();
            #endregion
        }

        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            Suku expected = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockDalSuku.Expect(x => x.GetById("A")).Return(expected);
            #endregion

            #region ACT
            Suku actual = blSuku.GetById("A");
            #endregion

            #region ASSERT
            mockDalSuku.VerifyAllExpectations();
            #endregion
        }

        [TestMethod]
        public void LIstAll_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            List<Suku> expected = new List<Suku>
            {
                new Suku { Kode = "A", Nama = "Data1" },
                new Suku { Kode = "B", Nama = "Data2" }
            };
            mockDalSuku.Expect(x => x.ListAll()).Return(expected);
            #endregion

            #region ACT
            List<Suku> actual = blSuku.ListAll();
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual);
            #endregion
        }
    }
}
