﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHospital.Billing.DataSosial.Dal;
using MyHospital.UnitTest.Helper;
using System.Data.SqlClient;
using MyHospital.Billing.DataSosial;
using System.Collections.Generic;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class PekerjaanTestDal
    {
        private PekerjaanDal dalPekerjaan;

        public PekerjaanTestDal()
        {
            dalPekerjaan = new PekerjaanDal();
            dalPekerjaan.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSQL = @"
                    DELETE  ta_pekerjaan_dk ";
                SqlCommand cmd = new SqlCommand(sSQL, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"
                    DELETE      ta_pekerjaan_dk ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void Insert_00_NewData_ExpectSucceed()
        {

            #region arrange
            Pekerjaan dataPekerjaan = new Pekerjaan
            {
                Kode = "A",
                Nama = "Pekerjaan1"
            };
            #endregion

            #region act
            dalPekerjaan.Insert(dataPekerjaan);
            #endregion

            #region assert

            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_DuplicateData_ExpectSqlException()
        {
            #region arrange
            Pekerjaan dataPekerjaan1 = new Pekerjaan
            {
                Kode = "A",
                Nama = "Pekerjaan1"
            };
            dalPekerjaan.Insert(dataPekerjaan1);
            Pekerjaan dataPekerjaan2 = new Pekerjaan
            {
                Kode = "A",
                Nama = "Pekerjaan2"
            };
            #endregion

            #region act
            dalPekerjaan.Insert(dataPekerjaan2);
            #endregion

            #region assert
            
            #endregion
        }

        [TestMethod]
        public void Update_00_DataExist_ExpectSucceed()
        {
            #region arrange
            Pekerjaan dataPekerjaan1 = new Pekerjaan
            {
                Kode = "A",
                Nama = "Pekerjaan1"
            };
            dalPekerjaan.Insert(dataPekerjaan1);

            Pekerjaan dataPekerjaan2 = new Pekerjaan
            {
                Kode = "A",
                Nama = "Pekerjaan2"
            };
            #endregion

            #region act
            dalPekerjaan.Update(dataPekerjaan2);
            #endregion

            #region assert

            #endregion
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            #region arrange
            Pekerjaan dataPekerjaan = new Pekerjaan
            {
                Kode = "A",
                Nama = "Pekerjaan1"
            };
            dalPekerjaan.Insert(dataPekerjaan);
            #endregion

            #region act
            dalPekerjaan.Delete(dataPekerjaan);
            #endregion

            #region assert

            #endregion
        }

        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {
            #region arrange
            Pekerjaan expected = new Pekerjaan
            {
                Kode = "A",
                Nama = "Pekerjaan1"
            };
            dalPekerjaan.Insert(expected);
            #endregion

            #region act
            Pekerjaan actual = dalPekerjaan.GetById("A");
            #endregion

            #region assert
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama); 
            #endregion
        }

        [TestMethod]
        public void ListAll_00_DataExist_ExpectSucceed()
        {
            #region arrange
            Pekerjaan expected1 = new Pekerjaan
            {
                Kode = "A",
                Nama = "Pekerjaan1"
            };
            dalPekerjaan.Insert(expected1);
            Pekerjaan expected2 = new Pekerjaan
            {
                Kode = "B",
                Nama = "Pekerjaan2"
            };
            dalPekerjaan.Insert(expected2); 
            #endregion

            #region act
            List<Pekerjaan> actual = dalPekerjaan.ListAll(); 
            #endregion

            #region assert
            Pekerjaan actual1 = actual.Find(x => x.Kode == "A");

            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama); 
            #endregion
        }
    }
}
