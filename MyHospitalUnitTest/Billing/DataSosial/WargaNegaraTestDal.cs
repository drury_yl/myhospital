﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial;
using MyHospital.UnitTest.Helper;
using System.Data.SqlClient;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class WargaNegaraDalTests
    {
        private WargaNegaraDal dalWargaNegara;
        public WargaNegaraDalTests()
        {
            dalWargaNegara = new WargaNegaraDal();
            dalWargaNegara.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using(SqlConnection conn= new SqlConnection())
            {
                String ssql = @"Delete from ta_warga_negara ";
                SqlCommand cmd = new SqlCommand(ssql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
        [TestCleanup]

        public void TestCleanup()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                String ssql = @"Delete from ta_warga_negara ";
                SqlCommand cmd = new SqlCommand(ssql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void Insert_00_NonExistedData_ExpectSucced()
        {
            #region ARRANGE
            WargaNegara dataWargaNegara = new WargaNegara
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            dalWargaNegara.Insert(dataWargaNegara);
            #endregion

            #region ASSERT
            // expect succed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_ExistedData_ExpectSqlException()
        {
            #region ARRANGE
            WargaNegara dataWargaNegara1 = new WargaNegara
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalWargaNegara.Insert(dataWargaNegara1);
            WargaNegara dataWargaNegara2 = new WargaNegara
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            dalWargaNegara.Insert(dataWargaNegara2);
            #endregion

            #region ASSERT
            // Expect Exception, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            WargaNegara dataWargaNegara1 = new WargaNegara
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalWargaNegara.Insert(dataWargaNegara1);
            WargaNegara dataWargaNegara2 = new WargaNegara
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            dalWargaNegara.Update(dataWargaNegara2);
            #endregion

            #region VERIFY
            // expect succed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_01_NonExistedData_ExpectSucceed()
        {
            #region INIT
            WargaNegara dataWargaNegara1 = new WargaNegara
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region CALLING
            dalWargaNegara.Update(dataWargaNegara1);
            #endregion

            #region VERIFY
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {

            #region ARRANGE
            WargaNegara dataWargaNegara = new WargaNegara
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalWargaNegara.Insert(dataWargaNegara);
            #endregion

            #region ACT
            dalWargaNegara.Delete(dataWargaNegara);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_01_NonExistedData_ExpectSucceed()
        {

            #region ARRANGE
            WargaNegara dataWargaNegara = new WargaNegara
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            dalWargaNegara.Delete(dataWargaNegara);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void GetById_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            WargaNegara expected = new WargaNegara
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalWargaNegara.Insert(expected);
            #endregion

            #region ACT
            WargaNegara actual = dalWargaNegara.GetById("A");
            #endregion

            #region ASSERT
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            #endregion
        }

        [TestMethod]
        public void ListAll_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            WargaNegara expected1 = new WargaNegara
            {
                Kode = "A",
                Nama = "Data1"
            };
            WargaNegara expected2 = new WargaNegara
            {
                Kode = "B",
                Nama = "Data2"
            };
            dalWargaNegara.Insert(expected1);
            dalWargaNegara.Insert(expected2);
            #endregion

            #region ACT
            List<WargaNegara> actual = dalWargaNegara.ListAll();
            #endregion

            #region ASSERT
            WargaNegara actual1 = actual.Find(x => x.Kode == expected1.Kode);
            WargaNegara actual2 = actual.Find(x => x.Kode == expected2.Kode);
            //
            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
            Assert.AreEqual(expected2.Kode, actual2.Kode);
            Assert.AreEqual(expected2.Nama, actual2.Nama);
            #endregion
        }

    }
}
