﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial.Ci;
using MyHospital.Billing.DataSosial.Bl;
using MyHospital.Billing.DataSosial;

namespace MyHospital.UnitTest.Billing.DataSosial.Bl
{
    [TestClass]
    public class WargaNegaraTestBl
    {
        private IWargaNegaraDal mockDalWargaNegara;
        private WargaNegaraBl blWargaNegara;

        [TestInitialize]
        public void TestInit()
        {
            mockDalWargaNegara = MockRepository.GenerateStub<IWargaNegaraDal>();
        }
    }
}
