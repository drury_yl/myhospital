﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHospital.Billing.DataSosial.Dal;
using MyHospital.UnitTest.Helper;
using System.Data.SqlClient;
using MyHospital.Billing.DataSosial;
using System.Collections.Generic;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class NegaraDalTest
    {
        private NegaraDal dalNegara;

        public NegaraDalTest()
        {
            dalNegara = new NegaraDal();
            dalNegara.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" TRUNCATE TABLE ta_negara";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" TRUNCATE TABLE ta_negara";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void Insert_00_NonExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Negara dataNegara = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            #endregion

            #region ACT
            dalNegara.Insert(dataNegara);
            #endregion

            #region ASSERT
            // expect succed on void procedure, no assert needed
            #endregion

        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_ExistedData_ExpectSqlException()
        {
            #region ARRANGE
            Negara dataNegara1 = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            dalNegara.Insert(dataNegara1);
            Negara dataNegara2 = new Negara
            {
                Kode = "A",
                Nama = "Negara2"
            };
            #endregion

            #region ACT
            dalNegara.Insert(dataNegara2);
            #endregion

            #region ASSERT
            // Expect Exception, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Negara dataNegara1 = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            dalNegara.Insert(dataNegara1);
            Negara dataNegara2 = new Negara
            {
                Kode = "A",
                Nama = "Negara2"
            };
            #endregion

            #region ACT
            dalNegara.Update(dataNegara2);
            #endregion

            #region VERIFY
            // expect succed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_01_NonExistedData_ExpectSucceed()
        {
            #region INIT
            Negara dataNegara1 = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            #endregion

            #region CALLING
            dalNegara.Update(dataNegara1);
            #endregion

            #region VERIFY
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {

            #region ARRANGE
            Negara dataNegara = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            dalNegara.Insert(dataNegara);
            #endregion

            #region ACT
            dalNegara.Delete(dataNegara);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_01_NonExistedData_ExpectSucceed()
        {

            #region ARRANGE
            Negara dataNegara = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            #endregion

            #region ACT
            dalNegara.Delete(dataNegara);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void GetById_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Negara expected = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            dalNegara.Insert(expected);
            #endregion

            #region ACT
            Negara actual = dalNegara.GetById("A");
            #endregion

            #region ASSERT
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            #endregion
        }

        [TestMethod]
        public void ListAll_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Negara expected1 = new Negara
            {
                Kode = "A",
                Nama = "Negara1"
            };
            Negara expected2 = new Negara
            {
                Kode = "B",
                Nama = "Negara2"
            };
            dalNegara.Insert(expected1);
            dalNegara.Insert(expected2);
            #endregion

            #region ACT
            List<Negara> actual = dalNegara.ListAll();
            #endregion

            #region ASSERT
            Negara actual1 = actual.Find(x => x.Kode == expected1.Kode);
            //
            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
            #endregion
        }
    }
}
