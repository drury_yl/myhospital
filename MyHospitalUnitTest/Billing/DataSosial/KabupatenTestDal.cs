﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Data.SqlClient;

using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial;
using MyHospital.UnitTest.Helper;

namespace MyHospital.UnitTest.Billing.DataSosial
{

    [TestClass]
    public class KabupatenDalTests
    {
        private KabupatenDal dalKabupaten;

        public KabupatenDalTests()
        {
            // Overwrite Connection string utk menghindari "kecelakaan" read-write database aktif
            dalKabupaten = new KabupatenDal();
            dalKabupaten.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_kabupaten";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();

                sSql = @" DELETE ta_propinsi";
                cmd = new SqlCommand(sSql, conn);
                cmd.ExecuteNonQuery();

                //  insert data pendukung untuk class composit-nya
                sSql = @" INSERT    ta_propinsi (fs_kd_propinsi, fs_nm_propinsi)
                          VALUES    @Kode, @Nama ";
                cmd.Parameters.Add(new SqlParameter("@Kode", "A"));
                cmd.Parameters.Add(new SqlParameter("@Nama", "Data1"));
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_kabupaten";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();

                sSql = @" DELETE ta_propinsi";
                cmd = new SqlCommand(sSql, conn);
                cmd.ExecuteNonQuery();
            }
        }


        [TestMethod]
        public void Insert_00_NonExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            #endregion

            #region ACT
            dalKabupaten.Insert(dataKabupaten);
            #endregion

            #region ASSERT
            // expect succed on void procedure, no assert needed
            #endregion

        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_ExistedData_ExpectSqlException()
        {
            #region ARRANGE
            Kabupaten dataKabupaten1 = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            dalKabupaten.Insert(dataKabupaten1);
            Kabupaten dataKabupaten2 = new Kabupaten
            {
                Kode = "A",
                Nama = "Data2",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            #endregion

            #region ACT
            dalKabupaten.Insert(dataKabupaten2);
            #endregion

            #region ASSERT
            // Expect Exception, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kabupaten dataKabupaten1 = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            dalKabupaten.Insert(dataKabupaten1);
            Kabupaten dataKabupaten2 = new Kabupaten
            {
                Kode = "A",
                Nama = "Data2",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            #endregion

            #region ACT
            dalKabupaten.Update(dataKabupaten2);
            #endregion

            #region VERIFY
            // expect succed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_01_NonExistedData_ExpectSucceed()
        {
            #region INIT
            Kabupaten dataKabupaten1 = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            #endregion

            #region CALLING
            dalKabupaten.Update(dataKabupaten1);
            #endregion

            #region VERIFY
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {

            #region ARRANGE
            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            dalKabupaten.Insert(dataKabupaten);
            #endregion

            #region ACT
            dalKabupaten.Delete(dataKabupaten);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_01_NonExistedData_ExpectSucceed()
        {

            #region ARRANGE
            Kabupaten dataKabupaten = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            #endregion

            #region ACT
            dalKabupaten.Delete(dataKabupaten);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void GetById_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kabupaten expected = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            dalKabupaten.Insert(expected);
            #endregion

            #region ACT
            Kabupaten actual = dalKabupaten.GetById("A");
            #endregion

            #region ASSERT
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            Assert.AreEqual(expected.Propinsi.Kode, actual.Propinsi.Kode);
            #endregion
        }

        [TestMethod]
        public void ListAll_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kabupaten expected1 = new Kabupaten
            {
                Kode = "A",
                Nama = "Data1",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            Kabupaten expected2 = new Kabupaten
            {
                Kode = "B",
                Nama = "Data2",
                Propinsi = new Propinsi
                {
                    Kode = "A"
                }
            };
            dalKabupaten.Insert(expected1);
            dalKabupaten.Insert(expected2);
            #endregion

            #region ACT
            List<Kabupaten> actual = dalKabupaten.ListAll();
            #endregion

            #region ASSERT
            Kabupaten actual1 = actual.Find(x => x.Kode == expected1.Kode);
            Kabupaten actual2 = actual.Find(x => x.Kode == expected2.Kode);
            //
            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
            Assert.AreEqual(expected1.Propinsi.Kode, actual1.Propinsi.Kode);
            Assert.AreEqual(expected2.Kode, actual2.Kode);
            Assert.AreEqual(expected2.Nama, actual2.Nama);
            Assert.AreEqual(expected2.Propinsi.Kode, actual2.Propinsi.Kode);
            #endregion
        }
    }
}
