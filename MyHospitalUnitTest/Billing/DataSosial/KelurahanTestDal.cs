﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHospital.Billing.DataSosial;
using MyHospital.Billing.DataSosial.Dal;
using System.Data.SqlClient;
using MyHospital.UnitTest.Helper;
using System.Collections.Generic;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class KelurahanTestDal
    {
        private KelurahanDal dalKelurahan;

        public KelurahanTestDal()
        {
            dalKelurahan = new KelurahanDal();
            dalKelurahan.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"
                                DELETE ta_kelurahan";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"
                                DELETE ta_kelurahan";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void Insert_00_NewData_ExpectSucceed()
        {
            //arrange
            Kelurahan dataKelurahan = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan1"
            };

            //act
            dalKelurahan.Insert(dataKelurahan);

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_DuplicateKode_ExpectSqlException()
        {
            //arrange
            Kelurahan dataKelurahan1 = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan1"
            };
            dalKelurahan.Insert(dataKelurahan1);
            Kelurahan dataKelurahan2 = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan2"
            };

            //act
            dalKelurahan.Insert(dataKelurahan2);

            //assert
        }

        [TestMethod]
        public void Update_00_DataExist_ExpectSucceed()
        {
            //arrange
            Kelurahan dataKelurahan1 = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan1"
            };
            dalKelurahan.Insert(dataKelurahan1);

            Kelurahan dataKelurahan2 = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan2"
            };            

            //act
            dalKelurahan.Update(dataKelurahan2);

            //assert
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            //arange
            Kelurahan dataKelurahan = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan1"
            };
            dalKelurahan.Insert(dataKelurahan);

            //act
            dalKelurahan.Delete(dataKelurahan);

            //assert
        }

        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {
            //arrange
            Kelurahan expected = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan1"
            };
            dalKelurahan.Insert(expected);

            //act
            Kelurahan actual = dalKelurahan.GetById("A");

            //assert
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
        }

        [TestMethod]
        public void ListAll_00_DataExist_Expect_Succeed()
        {
            //arrange
            Kelurahan expected1 = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan1"
            };
            dalKelurahan.Insert(expected1);
            Kelurahan expected2 = new Kelurahan
            {
                Kode = "A",
                Nama = "Kelurahan1"
            };
            dalKelurahan.Insert(expected2);

            //act
            List<Kelurahan> actual = dalKelurahan.ListAll();

            //assert
            Kelurahan actual1 = actual.Find(x => x.Kode == "A");

            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
        }
    }
}
