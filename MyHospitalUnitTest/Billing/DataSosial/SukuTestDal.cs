﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MyHospital.Billing.DataSosial;
using MyHospital.UnitTest.Helper;

namespace MyHospital.UnitTest.Billing.DataSosial
{

    [TestClass]
    public class SukuDalTests
    {
        private SukuDal dalSuku;

        public SukuDalTests()
        {
            // Overwrite Connection string utk menghindari "kecelakaan" read-write database aktif
            dalSuku = new SukuDal();
            dalSuku.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_suku";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_suku";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }


        [TestMethod]
        public void Insert_00_NonExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Suku dataSuku = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            dalSuku.Insert(dataSuku);
            #endregion

            #region ASSERT
            // expect succed on void procedure, no assert needed
            #endregion

        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_ExistedData_ExpectSqlException()
        {
            #region ARRANGE
            Suku dataSuku1 = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalSuku.Insert(dataSuku1);
            Suku dataSuku2 = new Suku
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            dalSuku.Insert(dataSuku2);
            #endregion

            #region ASSERT
            // Expect Exception, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Suku dataSuku1 = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalSuku.Insert(dataSuku1);
            Suku dataSuku2 = new Suku
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            dalSuku.Update(dataSuku2);
            #endregion

            #region VERIFY
            // expect succed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_01_NonExistedData_ExpectSucceed()
        {
            #region INIT
            Suku dataSuku1 = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region CALLING
            dalSuku.Update(dataSuku1);
            #endregion

            #region VERIFY
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {

            #region ARRANGE
            Suku dataSuku = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalSuku.Insert(dataSuku);
            #endregion

            #region ACT
            dalSuku.Delete(dataSuku);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_01_NonExistedData_ExpectSucceed()
        {

            #region ARRANGE
            Suku dataSuku = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            dalSuku.Delete(dataSuku);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void GetById_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Suku expected = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalSuku.Insert(expected);
            #endregion

            #region ACT
            Suku actual = dalSuku.GetById("A");
            #endregion

            #region ASSERT
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            #endregion
        }

        [TestMethod]
        public void ListAll_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Suku expected1 = new Suku
            {
                Kode = "A",
                Nama = "Data1"
            };
            Suku expected2 = new Suku
            {
                Kode = "B",
                Nama = "Data2"
            };
            dalSuku.Insert(expected1);
            dalSuku.Insert(expected2);
            #endregion

            #region ACT
            List<Suku> actual = dalSuku.ListAll();
            #endregion

            #region ASSERT
            Suku actual1 = actual.Find(x => x.Kode == expected1.Kode);
            Suku actual2 = actual.Find(x => x.Kode == expected2.Kode);
            //
            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
            Assert.AreEqual(expected2.Kode, actual2.Kode);
            Assert.AreEqual(expected2.Nama, actual2.Nama);
            #endregion
        }
    }
}
