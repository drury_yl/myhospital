﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

using Ics.Helper7.BaseClass;

using MyHospital.Billing.DataSosial.Ci;
using MyHospital.Billing.DataSosial.Bl;
using MyHospital.Billing.DataSosial;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class PropinsiTestBl
    {

        private IPropinsiDal mockDalPropinsi;
        private PropinsiBl blPropinsi;
        

        [TestInitialize]
        public void TestInit()
        {
            mockDalPropinsi = MockRepository.GenerateStub<IPropinsiDal>();
            blPropinsi = new PropinsiBl()
            {
                DalPropinsi = mockDalPropinsi
            };
        }

        [TestCleanup]
        public void TestCleanUp()
        {

        }

        [TestMethod]
        public void Save_00_ValidData_ExpectSucceed()
        {
            #region ARRANGE
            //  expected no error :-D
            int expected = 0;

            // stub object yang tidak di-test
            Propinsi fakePropinsi = null;
            mockDalPropinsi.Stub(x => x.GetById("A")).Return(fakePropinsi);

            Propinsi dataPropinsi = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blPropinsi.Save(dataPropinsi);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Count);
            #endregion
        }

        [TestMethod]
        public void Save_01_KodePropinsiKosong_ExpectFailed_BR01()
        {
            #region ARRANGE
            // set expected value
            string expected = "BR-01";
            //  stub object yang tidak di-test
            Propinsi fakePropinsi = new Propinsi { Kode = " ", Nama = " " };
            mockDalPropinsi.Stub(x => x.GetById("A")).Return(fakePropinsi);
            //
            Propinsi dataPropinsi = new Propinsi
            {
                Kode = " ",
                Nama = "Data1"
            };
            #endregion


            #region ACT
            MethodRetVal actual = blPropinsi.Save(dataPropinsi);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_02_NamaPropinsiKosong_ExpectFailed_BR02()
        {
            #region ARRANGE
            // set expected value
            string expected = "BR-02";

            //  stub object yang tidak di-test
            Propinsi fakePropinsi = new Propinsi { Kode = " ", Nama = " " };
            mockDalPropinsi.Stub(x => x.GetById("A")).Return(fakePropinsi);

            Propinsi dataPropinsi = new Propinsi
            {
                Kode = "A",
                Nama = " "
            };
            #endregion

            #region ACT
            MethodRetVal actual = blPropinsi.Save(dataPropinsi);
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual.Items.Find(x => x.ErrId == expected).ErrId);
            #endregion
        }

        [TestMethod]
        public void Save_03_NewDataCallInsert_ExpectSucceed()
        {
            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalPropinsi.Stub(x => x.GetById("A")).Return(null);

            Propinsi dataPropinsi = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blPropinsi.Save(dataPropinsi);
            #endregion

            #region ASSERT
            mockDalPropinsi.AssertWasCalled(x => x.Insert((dataPropinsi)));
            mockDalPropinsi.AssertWasNotCalled(x => x.Update((dataPropinsi)));
            #endregion
        }

        [TestMethod]
        public void Save_04_EditDataCallUpdate_ExpectSucceed()
        {

            #region ARRANGE
            //  stub object yang tidak di-test
            mockDalPropinsi.Stub(x => x.GetById("A")).Return(new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            });
            Propinsi dataPropinsi = new Propinsi
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            MethodRetVal actual = blPropinsi.Save(dataPropinsi);
            #endregion

            #region ASSERT
            mockDalPropinsi.AssertWasCalled(x => x.Update((dataPropinsi)));
            mockDalPropinsi.AssertWasNotCalled(x => x.Insert((dataPropinsi)));
            #endregion
        }

        [TestMethod]
        public void Delete_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            Propinsi dataPropinsi = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockDalPropinsi.Expect(x => x.Delete(dataPropinsi));
            #endregion

            #region ACT
            MethodRetVal retVal = blPropinsi.Delete(dataPropinsi);
            #endregion

            #region ASSERT
            mockDalPropinsi.VerifyAllExpectations();
            #endregion
        }

        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            Propinsi expected = new Propinsi
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockDalPropinsi.Expect(x => x.GetById("A")).Return(expected);
            #endregion

            #region ACT
            Propinsi actual = blPropinsi.GetById("A");
            #endregion

            #region ASSERT
            mockDalPropinsi.VerifyAllExpectations(); 
            #endregion
        }

        [TestMethod]
        public void LIstAll_00_DataExist_ExpectSucceed()
        {
            #region ARRANGE
            List<Propinsi> expected = new List<Propinsi>
            {
                new Propinsi { Kode = "A", Nama = "Data1" },
                new Propinsi { Kode = "B", Nama = "Data2" }
            };
            mockDalPropinsi.Expect(x => x.ListAll()).Return(expected);
            #endregion

            #region ACT
            List<Propinsi> actual = blPropinsi.ListAll();
            #endregion

            #region ASSERT
            Assert.AreEqual(expected, actual); 
            #endregion
        }


    }
}