﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial;
using MyHospital.UnitTest.Helper;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class PendidikanDalTests
    {
        private PendidikanDal dalPendidikan;
        public PendidikanDalTests()
        {
            dalPendidikan = new PendidikanDal();
            dalPendidikan.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"DELETE ta_pendidikan";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup ]
        public void TestCleanUp()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @"DELETE ta_pendidikan";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void Insert_00_NonExistedData_ExpectSucceed()
        {
            #region
            Pendidikan dataPendidikan = new Pendidikan
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region
            dalPendidikan.Insert(dataPendidikan);
            #endregion

            #region
            //expect succeed
            #endregion

        }

        [TestMethod]
        [ExpectedException (typeof(SqlException))]
        public void Insert_01_ExistedData_ExpectSqlException()
        {
            #region Arrange
            Pendidikan dataPendidikan1 = new Pendidikan
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalPendidikan.Insert(dataPendidikan1);
            Pendidikan dataPendidikan2 = new Pendidikan
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region
            dalPendidikan.Insert(dataPendidikan2);
            #endregion

            #region
            //expect exception
            #endregion

        }
        [TestMethod]
        public void Update_00_ExistedData_ExpectSucceed()
        {
            #region
            Pendidikan dataPendidikan1 = new Pendidikan
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalPendidikan.Insert(dataPendidikan1);
            Pendidikan dataPendidikan2 = new Pendidikan
            {
                Kode = "A",
                Nama = "Data2"
            };
            #endregion

            #region
            dalPendidikan.Insert(dataPendidikan2);
            #endregion

            #region
            //expect succeed
            #endregion

        }

        [TestMethod]
        public void Update_01_NonExistedData_ExpectSucceed()
        {
            #region
            Pendidikan dataPendidikan1 = new Pendidikan
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region 
            dalPendidikan.Insert(dataPendidikan1);
            #endregion

            #region
            //expect succeed
            #endregion

        }
        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {
            #region
            Pendidikan dataPendidikan1 = new Pendidikan
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region
            dalPendidikan.Delete(dataPendidikan1);
            #endregion

            #region
            //expect succeed
            #endregion

        }

        [TestMethod]
        public void GetById_00_ExistedData_ExpectSucceed()
        {
            #region
            Pendidikan expected = new Pendidikan
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalPendidikan.Insert(expected);
            #endregion

            #region
            Pendidikan actual = dalPendidikan.GetById("A");
            #endregion

            #region
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            #endregion

        }

        [TestMethod]
        public void ListAll_00_ExistedData_ExpectSucceed()
        {
            #region
            Pendidikan expected1 = new Pendidikan
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalPendidikan.Insert(expected1);
            Pendidikan expected2 = new Pendidikan
            {
                Kode = "A",
                Nama = "Data2"
            };
            
            dalPendidikan.Insert(expected2);
            #endregion

            #region
            List<Pendidikan> actual = dalPendidikan.ListAll();
            #endregion

            #region
            Pendidikan actual1 = actual.Find(x => x.Kode == expected1.Kode);
            Pendidikan actual2 = actual.Find(x => x.Kode == expected2.Kode);
            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected2.Kode, actual2.Kode);
            #endregion

        }

    }
}
