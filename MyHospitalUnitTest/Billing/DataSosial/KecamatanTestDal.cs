﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHospital.Billing.DataSosial.Dal;
using MyHospital.UnitTest.Helper;
using System.Data.SqlClient;
using MyHospital.Billing.DataSosial;
using System.Collections.Generic;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class KecamatanDalTests
    {
        private KecamatanDal dalKecamatan;

        public KecamatanDalTests()
        {
            dalKecamatan = new KecamatanDal();
            dalKecamatan.OverwriteConnectionString(DbTestHelper.GetTestDbConnString());
        }

        [TestInitialize]
        public void TestInit()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_kecamatan";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            using (SqlConnection conn = new SqlConnection(DbTestHelper.GetTestDbConnString()))
            {
                string sSql = @" DELETE ta_kecamatan";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void Insert_00_NonExistData_ExpectSucceed()
        {
            #region ARRANGE
            Kecamatan dataKecamatan = new Kecamatan
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            dalKecamatan.Insert(dataKecamatan);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_01_ExistedData_ExpectSqlException()
        {
            #region ARRANGE
            Kecamatan dataKecamatan1 = new Kecamatan
            {
                Kode = "A",
                Nama = "Data1"
            };
            dalKecamatan.Insert(dataKecamatan1);

            Kecamatan dataKecamatan2 = new Kecamatan
            {
                Kode = "B",
                Nama = "Data2"
            };
            #endregion

            #region ACT
            dalKecamatan.Insert(dataKecamatan2);
            #endregion

            #region ASSERT
            // Expect Exception, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kecamatan dataKecamatan1 = new Kecamatan
            {
                Kode="A",
                Nama="Data1"
            };
            dalKecamatan.Insert(dataKecamatan1);

            Kecamatan dataKecamatan2 = new Kecamatan
            {
                Kode="B",
                Nama="Data2"
            };
            #endregion

            #region ACT
            dalKecamatan.Update(dataKecamatan2);
            #endregion

            #region ASSERT
            // Expect Exception, no assert needed
            #endregion
        }

        [TestMethod]
        public void Update_01_NonExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kecamatan dataKecamatan = new Kecamatan
            {
                Kode = "A",
                Nama = "Data1"
            };
            #endregion

            #region ACT
            dalKecamatan.Update(dataKecamatan);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void Delete_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kecamatan dataKecamatan = new Kecamatan
            {
                Kode="A",
                Nama="Data1"
            };
            dalKecamatan.Insert(dataKecamatan);
            #endregion

            #region ACT
            dalKecamatan.Delete(dataKecamatan);
            #endregion

            #region ASSERT
            // expect succeed on void procedure, no assert needed
            #endregion
        }

        [TestMethod]
        public void GetById_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kecamatan expected = new Kecamatan
            {
                Kode="A",
                Nama="Data1"
            };
            dalKecamatan.Insert(expected);
            #endregion

            #region ACT
            Kecamatan actual = dalKecamatan.GetById("A");
            #endregion

            #region ASSERT
            Assert.AreEqual(expected.Kode, actual.Kode);
            Assert.AreEqual(expected.Nama, actual.Nama);
            #endregion
        }

        [TestMethod]
        public void ListAll_00_ExistedData_ExpectSucceed()
        {
            #region ARRANGE
            Kecamatan expected1 = new Kecamatan
            {
                Kode="A",
                Nama="Data1"
            };
            Kecamatan expected2 = new Kecamatan
            {
                Kode="B",
                Nama="Data2"
            };
            dalKecamatan.Insert(expected1);
            dalKecamatan.Insert(expected2);
            #endregion

            #region ACT
            List<Kecamatan> actual = dalKecamatan.ListAll();
            #endregion

            #region ASSERT
            Kecamatan actual1 = actual.Find(x => x.Kode == expected1.Kode);
            Kecamatan actual2 = actual.Find(x => x.Kode == expected2.Kode);

            Assert.AreEqual(expected1.Kode, actual1.Kode);
            Assert.AreEqual(expected1.Nama, actual1.Nama);
            Assert.AreEqual(expected2.Kode, actual2.Kode);
            Assert.AreEqual(expected2.Nama, actual2.Nama);
            #endregion
        }
    }
}
