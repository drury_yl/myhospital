﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHospital.Billing.DataSosial;

using Rhino.Mocks;
using Ics.Helper7.BaseClass;

namespace MyHospital.UnitTest.Billing.DataSosial
{
    [TestClass]
    public class AgamaTestBl
    {
        private AgamaBl blAgama;
        private IAgamaDal mockAgamaDal;

        [TestInitialize]
        public void TestInit()
        {
            mockAgamaDal = MockRepository.GenerateStub<IAgamaDal>();
            blAgama = new AgamaBl
            {
                DalAgama = mockAgamaDal
            };
        }


        [TestCleanup]
        public void TestCleanUp()
        {

        }



        [TestMethod]
        public void Save_00_NewData_ExpectSucceed()
        {
            // arrange
            mockAgamaDal.Stub(x => x.GetById("A")).Return(null);

            Agama dataAgama = new Agama
            {
                Kode = "A",
                Nama = "Data1"
            };

            // act
            MethodRetVal actual = blAgama.Save(dataAgama);

            // assert
            mockAgamaDal.AssertWasCalled(x => x.Insert(dataAgama));
            mockAgamaDal.AssertWasNotCalled(x => x.Update(dataAgama));
        }

        [TestMethod]
        public void Save_01_DataExist_ExpectSucceed()
        {
            // arrange
            Agama dataAgamaExist = new Agama
            {
                Kode = "A",
                Nama = "Data1"
            };
            mockAgamaDal.Stub(x => x.GetById("A")).Return(dataAgamaExist);

            Agama dataAgama = new Agama
            {
                Kode = "A",
                Nama = "Data2"
            };

            // act
            MethodRetVal actual = blAgama.Save(dataAgama);

            // assert
            mockAgamaDal.AssertWasCalled(x => x.Update(dataAgama));
            mockAgamaDal.AssertWasNotCalled(x => x.Insert(dataAgama));
        }

        [TestMethod]
        public void Save_02_KodeKosong_ExpectFailed_BR01()
        {

            //arrange
            string expected = "BR-01";

            Agama dataAgama = new Agama
            {
                Kode = "",
                Nama = "Agama1"
            };

            //act
            MethodRetVal actual = blAgama.Save(dataAgama);

            //assert

            Assert.IsTrue(actual.Items.Exists(x => x.ErrId == expected));
        }

        [TestMethod]
        public void Save_03_KodeKosong_ExpectFailed_BR02()
        {

            //arrange
            string expected = "BR-02";

            Agama dataAgama = new Agama
            {
                Kode = "A",
                Nama = ""
            };

            //act
            MethodRetVal actual = blAgama.Save(dataAgama);

            //assert

            Assert.IsTrue(actual.Items.Exists(x => x.ErrId == expected));
        }

        [TestMethod]
        public void Delete_00_KodeKosong_ExpectSucceed()
        {

            //arrange
            Agama dataAgama = new Agama
            {
                Kode = "A",
                Nama = "Agama1"
            };

            //act
            MethodRetVal actual = blAgama.Delete(dataAgama);

            //assert
            mockAgamaDal.AssertWasCalled(x => x.Delete(dataAgama));
        }


        [TestMethod]
        public void GetById_00_DataExist_ExpectSucceed()
        {

            //arrange

            Agama dataAgama = new Agama
            {
                Kode = "A",
                Nama = "Agama1"
            };
            mockAgamaDal.Expect(x => x.GetById("A")).Return(dataAgama);

            //act
            Agama actual = blAgama.GetById("A");

            //assert
            mockAgamaDal.VerifyAllExpectations();
        }


    }
}
