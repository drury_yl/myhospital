﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;


using MyHospital.Billing.DataSosial.Ci;

namespace MyHospital.Billing.DataSosial.Dal
{

    public class PropinsiDal : IPropinsiDal
    {

        private string connStr;

        public PropinsiDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }

        public void Insert(Propinsi entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    INSERT INTO    ta_propinsi (fs_kd_propinsi, fs_nm_propinsi)
                    VALUES         (@Kode, @Nama) ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Update(Propinsi entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    UPDATE      ta_propinsi
                    SET         fs_kd_propinsi = @Kode,
                                fs_nm_propinsi = @Nama
                    WHERE       fs_kd_propinsi = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Delete(Propinsi entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    DELETE     ta_propinsi
                    WHERE      fs_kd_propinsi = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public Propinsi GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    SELECT   fs_kd_propinsi, fs_nm_propinsi 
                    FROM     ta_propinsi 
                    WHERE    fs_kd_propinsi = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    Propinsi retVal = new Propinsi
                    {
                        Kode = dr["fs_kd_propinsi"].ToString(),
                        Nama = dr["fs_nm_propinsi"].ToString()
                    };
                    return retVal;
                }
                dr.Close();
            }
            return null;
        }

        public List<Propinsi> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    SELECT     fs_kd_propinsi, fs_nm_propinsi 
                    FROM       ta_propinsi ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    List<Propinsi> retVal = new List<Propinsi>();
                    while (dr.Read())
                    {
                        retVal.Add(new Propinsi
                        {
                            Kode = dr["fs_kd_propinsi"].ToString(),
                            Nama = dr["fs_nm_propinsi"].ToString(),
                        });
                    }
                    return retVal;
                }
                dr.Close();
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr;
        }
    }
}
