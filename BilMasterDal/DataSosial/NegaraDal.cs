﻿using MyHospital.Billing.DataSosial.Ci;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHospital.Billing.DataSosial.Dal
{
    public class NegaraDal : INegaraDal
    {
        private string connStr;

        public NegaraDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }


        public void Delete(Negara entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    DELETE  ta_negara
                    WHERE   fs_kd_negara = @Kode";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public Negara GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    SELECT  fs_kd_negara, fs_nm_negara
                    FROM    ta_negara
                    WHERE   fs_kd_negara = @Kode";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    Negara retval = new Negara
                    {
                        Kode = dr["fs_kd_negara"].ToString(),
                        Nama = dr["fs_nm_negara"].ToString()
                    };
                    return retval;
                }
                dr.Close();
            }
            return null;
        }

        public void Insert(Negara entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    INSERT INTO ta_negara (fs_kd_negara, fs_nm_negara)
                    VALUES      (@Kode, @Nama) ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Update(Negara entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    UPDATE      ta_negara 
                    SET         fs_kd_negara = @Kode,
                                fs_nm_negara = @Nama
                    VALUES      fs_kd_negara = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public List<Negara> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    SELECT  fs_kd_negara, fs_nm_negara
                    FROM    ta_negara";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    List<Negara> retval = new List<Negara>();
                    while (dr.Read())
                    {
                        retval.Add(new Negara
                        {
                            Kode = dr["fs_kd_negara"].ToString(),
                            Nama = dr["fs_nm_negara"].ToString()
                        });
                    };
                    return retval;
                }
                dr.Close();
        }
            return null;
        }

    public void OverwriteConnectionString(string connStr)
        {
            throw new NotImplementedException();
        }

    }
}
