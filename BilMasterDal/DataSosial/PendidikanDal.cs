﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MyHospital.Billing.DataSosial.Ci;

namespace MyHospital.Billing.DataSosial.Dal
{
    public class PendidikanDal : IPendidikanDal
    {
        public string connStr;
        public PendidikanDal()
        {
            connStr = "Data Source=XSERVER, User Catalog= HOSPITAL_TEST, USer Id=sa, Password=b35mart1c5";
        }

        public void Insert(Pendidikan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSQL = @"
                        INSERT INTO ta_pendidikan (fs_kd_pendidikan, fs_nm_pendidikan)
                        VALUES   (@Kode, @Nama) ";
                SqlCommand cmd = new SqlCommand(sSQL, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }
        public void Update(Pendidikan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                       UPDATE       ta_pendidikan      
                       SET          fs_kd_pendidikan = @Kode,
                                    fs_nm_pendidikan = @Nama 
                       WHERE        fs_kd_pendidikan = @Kode ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };

        }

        public void Delete(Pendidikan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    DELETE      ta_pendidikan  
                    WHERE       fs_kd_pendidikan = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public Pendidikan GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    SELECT      fs_kd_pendidikan, fs_nm_pendidikan  
                    FROM        ta_pendidikan  
                    WHERE       fs_kd_pendidikan = @Kode ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    Pendidikan retVal = new Pendidikan
                    {
                        Kode = dr["fs_kd_pendidikan"].ToString(),
                        Nama = dr["fs_nm_pendidikan"].ToString()
                    };
                    return retVal;
                }
                dr.Close();
            }
            return null;
        }

        

        public List<Pendidikan> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    SELECT      fs_kd_pendidikan, fs_nm_pendidikan  
                    FROM        ta_pendidikan ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    List<Pendidikan> retVal = new List<Pendidikan>();

                    while (dr.Read())
                    {
                        retVal.Add(new Pendidikan
                        {
                            Kode = dr["fs_kd_pendidikan"].ToString(),
                            Nama = dr["fs_nm_pendidikan"].ToString()
                        });
                    }
                    return retVal;
                }
                dr.Close();
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr= connStr ;
        }

        
    }
}

