﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using MyHospital.Billing.DataSosial.Ci;

namespace MyHospital.Billing.DataSosial
{

    public class SukuDal : ISukuDal
    {

        private string connStr;

        public SukuDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }

        public void Insert(Suku entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    INSERT INTO    ta_suku (fs_kd_suku, fs_nm_suku)
                    VALUES         (@Kode, @Nama) ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Update(Suku entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    UPDATE      ta_suku
                    SET         fs_kd_suku = @Kode,
                                fs_nm_suku = @Nama
                    WHERE       fs_kd_suku = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Delete(Suku entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    DELETE     ta_suku
                    WHERE      fs_kd_suku = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public Suku GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    SELECT   fs_kd_suku, fs_nm_suku 
                    FROM     ta_suku 
                    WHERE    fs_kd_suku = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    Suku retVal = new Suku
                    {
                        Kode = dr["fs_kd_suku"].ToString(),
                        Nama = dr["fs_nm_suku"].ToString()
                    };
                    return retVal;
                }
            }
            return null;
        }

        public List<Suku> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    SELECT     fs_kd_suku, fs_nm_suku 
                    FROM       ta_suku ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    List<Suku> retVal = new List<Suku>();
                    while (dr.Read())
                    {
                        retVal.Add(new Suku
                        {
                            Kode = dr["fs_kd_suku"].ToString(),
                            Nama = dr["fs_nm_suku"].ToString(),
                        });
                    }
                    return retVal;
                }
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr;
        }
    }
}
