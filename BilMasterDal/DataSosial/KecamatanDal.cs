﻿using MyHospital.Billing.DataSosial.Ci;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHospital.Billing.DataSosial.Dal
{
    public class KecamatanDal : IKecamatanDal
    {
        private string connStr;

        public KecamatanDal()
        {
            connStr = "Data Source = XSERVER; Initial Catalog = HOSPITAL_TEST; User ID = sa; Password = b35mart1c5?";
        }

        public void Delete(Kecamatan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    DELETE  ta_kecamatan
                    WHERE   fs_kd_kecamatan = @Kode ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public Kecamatan GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    SELECT      fs_kd_kecamatan, fs_nm_kecamatan
                    FROM        ta_kecamatan
                    WHERE       fs_kd_kecamatan = @Kode";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    Kecamatan retVal = new Kecamatan
                    {
                        Kode = dr["fs_kd_kecamatan"].ToString(),
                        Nama = dr["fs_nm_kecamatan"].ToString()
                    };
                    return retVal;
                }
                dr.Close();
            }
            return null;
        }

        public void Insert(Kecamatan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    INSERT INTO     ta_kecamatan(fs_kd_kecamatan, fs_nm_kecamatan)
                    VALUES          (@Kode, @Nama) ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public List<Kecamatan> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    SELECT  fs_kd_kecamata, fs_nm_kecamatan
                    FROM    ta_kecamatan ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();

                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    List<Kecamatan> retVal = new List<Kecamatan>();
                    while (dr.Read())
                    {
                        retVal.Add(new Kecamatan
                        {
                            Kode = dr["fs_kd_kecamatan"].ToString(),
                            Nama = dr["fs_nm_kecamatan"].ToString()
                        });
                    }
                }
                dr.Close();
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr;
        }

        public void Update(Kecamatan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                    UPDATE      ta_kecamatan
                    SET         fs_kd_kecamatan = @Kode, 
                                fs_nm_kecamatan = @Nama 
                    WHERE       fs_kd_kecamtan = @Kode";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
