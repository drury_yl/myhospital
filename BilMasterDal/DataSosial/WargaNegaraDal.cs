﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MyHospital.Billing.DataSosial.Ci;

namespace MyHospital.Billing.DataSosial
{
    public class WargaNegaraDal : IWargaNegaraDal
    {
        private String connStr;

        public WargaNegaraDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }

        public void Insert(WargaNegara entity)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                string ssql = @"Insert Into ta_warga_negara (fs_kd_warga_negara,fs_nm_warga_negara)
                              VALUES (@Kode,@Nama)";
                SqlCommand cmd = new SqlCommand(ssql,conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();


            };
        }
        public void Update(WargaNegara entity)
        {
            using(SqlConnection conn= new SqlConnection())
            {
                string ssql = @"UPDATE ta_warga_negara 
                            SET fs_kd_warga_negara=@Kode,
                                fs_nm_warga_negara=@Nama
                            WHERE fs_kd_warga_negara=@Kode ";

                SqlCommand cmd = new SqlCommand(ssql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(WargaNegara entity)
        {
            using(SqlConnection conn= new SqlConnection())
            {
                string ssql = @" Delete ta_warga_negara
                            Where fs_kd_warga_negara=@Kode ";
                SqlCommand cmd = new SqlCommand(ssql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();


            }
        }

        public WargaNegara GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                string ssql = @" 
                            select  fs_kd_warga_negara,fs_nm_warga_negara
                            WHERE   fs_kd_warga_negara=@Kode ";
                SqlCommand cmd = new SqlCommand(ssql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    WargaNegara retval = new WargaNegara{
                        Kode = dr["fs_kd_warga_negara"].ToString(),
                        Nama=dr["fs_nm_warga_negara"].ToString()
                    };
                    return retval;
                }
                dr.Close();
            }
            return null;
        }


        public List<WargaNegara> ListAll()
        {
            using(SqlConnection conn= new SqlConnection())
            {
                String ssql = @" Select fs_kd_warga_negara, fs_nm_warga_negara 
                                FROM    ta_warga_negara ";
                SqlCommand cmd = new SqlCommand(ssql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                List<WargaNegara> retval = new List<WargaNegara>();
                if (dr.HasRows)
                {
                    while (dr.Read()){
                        retval.Add(new WargaNegara
                        {
                            Kode = dr["fs_kd_warga_negara"].ToString(),
                            Nama = dr["fs_nm_warga_negara"].ToString()
                        });
                    }
                    return retval;
                }
                dr.Close();
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr;
        }

        
    }
}
