﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;
using System.Data.SqlClient;

namespace MyHospital.Billing.DataSosial.Dal
{
    public class PekerjaanDal : IPekerjaanDAL
    {
        private string connStr;

        public PekerjaanDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }

        public void Delete(Pekerjaan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSQL = @"
                    DELETE  ta_pekerjaan_dk
                    WHERE   fs_kd_pekerjaan_dk = @kode ";
                SqlCommand cmd = new SqlCommand(sSQL, conn);
                cmd.Parameters.Add(new SqlParameter("@kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public Pekerjaan GetById(string id)
        {
            using(SqlConnection conn = new SqlConnection(connStr))
            {
                string sSQL = @"
                    SELECT  fs_kd_pekerjaan_dk, fs_nm_pekerjaan_dk
                    FROM    ta_pekerjaan_dk
                    WHERE   fs_kd_pekerjaan_dk = @kode ";

                SqlCommand cmd = new SqlCommand(sSQL, conn);
                cmd.Parameters.Add(new SqlParameter("@kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    Pekerjaan Retval = new Pekerjaan
                    {
                        Kode = dr["fs_kd_pekerjaan_dk"].ToString(),
                        Nama = dr["fs_nm_pekerjaan_dk"].ToString()
                    };
                    return Retval;
                }
                dr.Close();
            }
            return null;
        }

        public void Insert(Pekerjaan entity)
        {
            using(SqlConnection conn = new SqlConnection(connStr))
            {
                string sSQL = @"
                    INSERT INTO ta_pekerjaan_dk (fs_kd_pekerjaan_dk, fs_nm_pekerjaan_dk)
                    ValUES      (@kode, @nama) ";

                SqlCommand cmd = new SqlCommand(sSQL, conn);
                cmd.Parameters.Add(new SqlParameter("@kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public List<Pekerjaan> ListAll()
        {
            using(SqlConnection conn = new SqlConnection(connStr))
            {
                string sSQL = @"
                    SELECT  fs_kd_pekerjaan_dk, fs_nm_pekerjaan_dk
                    FROM    ta_pekerjaan_dk ";

                SqlCommand cmd = new SqlCommand(sSQL, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    List<Pekerjaan> RetVal = new List<Pekerjaan>();
                    while (dr.Read())
                    {
                        RetVal.Add(new Pekerjaan
                        {
                            Kode = dr["fs_kd_pekerjaan_dk"].ToString(),
                            Nama = dr["fs_nm_pekerjaan_dk"].ToString()
                        });
                    }
                    return RetVal;
                }
                dr.Close();
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr;
        }

        public void Update(Pekerjaan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSQL = @"
                    UPDATE  ta_pekerjaan_dk
                    SET     fs_kd_pekerjaan_dk = @kode,
                            fs_nm_pekerjaan_dk = @nama
                    WHERE   fs_kd_pekerjaan_dk = @kode ";

                SqlCommand cmd = new SqlCommand(sSQL, conn);
                cmd.Parameters.Add(new SqlParameter("@kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }
    }
}
