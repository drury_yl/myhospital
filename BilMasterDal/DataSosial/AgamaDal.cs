﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace MyHospital.Billing.DataSosial.Dal
{

    public class AgamaDal : IAgamaDal
    {

        private string connStr;

        public AgamaDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }

        public void Insert(Agama entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    INSERT INTO    ta_agama (fs_kd_agama, fs_nm_agama)
                    VALUES         (@Kode, @Nama) ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Update(Agama entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    UPDATE      ta_agama
                    SET         fs_kd_agama = @Kode,
                                fs_nm_agama = @Nama
                    WHERE       fs_kd_agama = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Delete(Agama entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    DELETE     ta_agama
                    WHERE      fs_kd_agama = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public Agama GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    SELECT   fs_kd_agama, fs_nm_agama 
                    FROM     ta_agama 
                    WHERE    fs_kd_agama = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    Agama retVal = new Agama
                    {
                        Kode = dr["fs_kd_agama"].ToString(),
                        Nama = dr["fs_nm_agama"].ToString()
                    };
                    return retVal;
                }
            }
            return null;
        }

        public List<Agama> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    SELECT     fs_kd_agama, fs_nm_agama 
                    FROM       ta_agama ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    List<Agama> retVal = new List<Agama>();
                    while (dr.Read())
                    {
                        retVal.Add(new Agama
                        {
                            Kode = dr["fs_kd_agama"].ToString(),
                            Nama = dr["fs_nm_agama"].ToString(),
                        });
                    }
                    return retVal;
                }
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr;
        }
    }
}
