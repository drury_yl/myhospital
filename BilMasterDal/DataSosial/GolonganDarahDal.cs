﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyHospital.Billing.DataSosial.Ci;

namespace MyHospital.Billing.DataSosial.Dal
{
    public class GolonganDarahDal : IGolonganDarahDal
    {
        public string connStr;

        public GolonganDarahDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }
        public void Insert(GolonganDarah entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                        INSERT INTO TA_GOL_DARAH 
                        VALUES(@Kode, @Nama) ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            }

        }

        public void Update(GolonganDarah entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    UPDATE      ta_gol_darah 
                    SET         fs_nm_gol_darah = @Nama
                    WHERE       fs_kd_gol_darah = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Delete(GolonganDarah entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    DELETE     ta_gol_darah
                    WHERE      fs_kd_gol_darah = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }
        public GolonganDarah GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                    SELECT   fs_kd_gol_darah, fs_nm_gol_darah
                    FROM     ta_gol_darah 
                    WHERE    fs_kd_gol_darah = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    GolonganDarah retVal = new GolonganDarah 
                    {
                        Kode = dr["fs_kd_gol_darah"].ToString(),
                        Nama = dr["fs_nm_gol_darah"].ToString()
                    };
                    return retVal;
                }
                dr.Close();
            }
            return null;
        }


        public List<GolonganDarah> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {

                string sSql = @" 
                    select      fs_kd_gol_darah, fs_nm_gol_darah 
                    from        ta_gol_darah
                    ORDER BY    fs_kd_gol_darah ";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader(); 
                
                if(dr.HasRows)
                {
                    List<GolonganDarah> retVal = new List<GolonganDarah>();
                    while (dr.Read())
                    {
                        retVal.Add(new GolonganDarah
                        {
                            Kode = dr["fs_kd_gol_darah"].ToString(),
                            Nama = dr["fs_kd_nm_darah"].ToString()
                        });
                    }
                        
                }
                dr.Close(); 
                                
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr;
        }
    }
}
