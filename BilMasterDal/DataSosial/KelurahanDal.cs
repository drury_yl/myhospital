﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;
using System.Data.SqlClient;

namespace MyHospital.Billing.DataSosial.Dal
{
    public class KelurahanDal : IKelurahanDal
    {
        private string connStr = "";

        public KelurahanDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }        
        public void Delete(Kelurahan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                            DELETE ta_kelurahan
                            WHERE fs_kd_kelurahan = @Kode";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }        
        
        public Kelurahan GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                            SELECT  fs_kd_kelurahan, fs_nm_kelurahan
                            FROM    ta_kelurahan
                            WHERE   fs_kd_kelurahan = @Kode";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    Kelurahan retVal = new Kelurahan
                    {
                        Kode = dr["fs_kd_kelurahan"].ToString(),
                        Nama = dr["fs_nm_kelurahan"].ToString()
                    };
                    return retVal;
                }                                      
            }
            return null;
        }        

        public void Insert(Kelurahan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                            INSERT INTO ta_kelurahan (fs_kd_kelurahan,fs_nm_kelurahan)
                            VALUES                    (@Kode, @Nama)";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();                
            };
        }

        public List<Kelurahan> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                            SELECT  fs_kd_kelurahan, fs_nm_kelurahan
                            FROM    ta_kelurahan";
                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {                    
                    List<Kelurahan> retVal = new List<Kelurahan>();
                    while (dr.Read())
                    {
                        retVal.Add(new Kelurahan
                        {
                            Kode = dr["fs_kd_kelurahan"].ToString(),
                            Nama = dr["fs_nm_kelurahan"].ToString()
                        });                        
                    }
                    return retVal;
                }
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr; 
        }

        public void Update(Kelurahan entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"
                            UPDATE  ta_kelurahan 
                            SET     fs_kd_kelurahan = @Kode, 
                                    fs_nm_kelurahan = @Nama
                            WHERE   fs_kd_kelurahan = @Kode";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }
    }
}
