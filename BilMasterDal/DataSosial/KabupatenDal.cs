﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlClient;
using MyHospital.Billing.DataSosial.Ci;

namespace MyHospital.Billing.DataSosial.Dal
{

    public class KabupatenDal : IKabupatenDal
    {

        private string connStr;

        public KabupatenDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }

        public void Insert(Kabupaten entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                INSERT INTO    ta_kabupaten (fs_kd_kabupaten, fs_nm_kabupaten,
                                             fs_kd_propinsi)
                VALUES         (@Kode, @Nama, @KodePropinsi) ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                cmd.Parameters.Add(new SqlParameter("@KodePropinsi", entity.Propinsi.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Update(Kabupaten entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                UPDATE      ta_kabupaten
                SET         fs_kd_kabupaten = @Kode,
                            fs_nm_kabupaten = @Nama,
                            fs_kd_propinsi = @KodePropinsi
                WHERE       fs_kd_kabupaten = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                cmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                cmd.Parameters.Add(new SqlParameter("@KodePropinsi", entity.Propinsi.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public void Delete(Kabupaten entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                DELETE     ta_kabupaten
                WHERE      fs_kd_kabupaten = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                cmd.ExecuteNonQuery();
            };
        }

        public Kabupaten GetById(string id)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                SELECT      aa.fs_kd_kabupaten, aa.fs_nm_kabupaten,
                            aa.fs_kd_propinsi, 
                            ISNULL(bb.fs_nm_propinsi, ' ') fs_nm_propinsi 
                FROM        ta_kabupaten aa
                LEFT JOIN   ta_propinsi bb ON aa.fs_kd_propinsi = bb.fs_kd_propinsi
                WHERE       fs_kd_kabupaten = @Kode ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                cmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    Kabupaten retVal = new Kabupaten
                    {
                        Kode = dr["fs_kd_kabupaten"].ToString(),
                        Nama = dr["fs_nm_kabupaten"].ToString(),
                        Propinsi = new Propinsi
                        {
                            Kode = dr["fs_kd_propinsi"].ToString(),
                            Nama = dr["fs_nm_propinsi"].ToString()
                        }
                    };
                    return retVal;
                }
            }
            return null;
        }

        public List<Kabupaten> ListAll()
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @" 
                SELECT      aa.fs_kd_kabupaten, aa.fs_nm_kabupaten,
                            aa.fs_kd_propinsi,
                            ISNULL(bb.fs_nm_propinsi, ' ') fs_nm_propinsi
                FROM        ta_kabupaten aa 
                LEFT JOIN   ta_propinsi bb ON aa.fs_kd_propinsi = bb.fs_kd_propinsi ";

                SqlCommand cmd = new SqlCommand(sSql, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    List<Kabupaten> retVal = new List<Kabupaten>();
                    while (dr.Read())
                    {
                        retVal.Add(new Kabupaten
                        {
                            Kode = dr["fs_kd_kabupaten"].ToString(),
                            Nama = dr["fs_nm_kabupaten"].ToString(),
                            Propinsi = new Propinsi
                            {
                                Kode = dr["fs_kd_propinsi"].ToString(),
                                Nama = dr["fs_nm_propinsi"].ToString()
                            }
                        });
                    }
                    return retVal;
                }
            }
            return null;
        }

        public void OverwriteConnectionString(string connStr)
        {
            this.connStr = connStr;
        }
    }
}





