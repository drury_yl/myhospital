﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyHospital.Billing.DataSosial.Ci;
using System.Data.SqlClient;

namespace MyHospital.Billing.DataSosial.Dal
{
    public class StatusKawinDal : IStatusKawinDal

    {
        private string connStr;

        public StatusKawinDal()
        {
            connStr = "Data Source=XSERVER;Initial Catalog=HOSPITAL_TEST;User ID=sa;Password=b35mart1c5?";
        }

        public void Delete(StatusKawin entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"DELETE    ta_status_kawin_dk 
                                WHERE     fs_kd_status_kawin_dk = @Kode";
                SqlCommand sqlCmd = new SqlCommand(sSql, conn);
                sqlCmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                conn.Open();
                sqlCmd.ExecuteNonQuery();
            }
        }

        public StatusKawin GetById(string id)
        {
            using (SqlConnection conn=new SqlConnection(connStr))
            {
                string sSql = @"SELECT   fs_kd_status_kawin_dk, fs_nm_status_kawin_dk
                               FROM     ta_status_kawin_dk 
                               WHERE   fs_kd_status_kawin_dk = @Kode";
                SqlCommand sqlCmd = new SqlCommand(sSql, conn);
                sqlCmd.Parameters.Add(new SqlParameter("@Kode", id));
                conn.Open();
                SqlDataReader dr = sqlCmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    StatusKawin retval = new StatusKawin
                    {
                        Kode = dr["fs_kd_status_kawin_dk"].ToString(),
                        Nama = dr["fs_nm_status_kawin_dk"].ToString()
                    };
                    return retval;
                }
                return null;
            }
        }

        public void Insert(StatusKawin entity)
        {
            using (SqlConnection conn =new SqlConnection(connStr))
            {
                string sSql = @"INSERT INTO ta_status_kawin_dk (fs_kd_status_kawin_dk, fs_nm_status_kawin_dk)
                                VALUES      (@Kode, @Nama)";
                SqlCommand sqlCmd = new SqlCommand(sSql, conn);
                sqlCmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                sqlCmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                sqlCmd.ExecuteNonQuery();
            }
        }

        public List<StatusKawin> ListAll()
        {
            using (SqlConnection conn=new SqlConnection(connStr))
            {
                string sSql = @"SELECT    fs_kd_status_kawin_dk, fs_nm_status_kawin_dk
                            FROM      ta_status_kawin_dk";
                SqlCommand sqlCmd = new SqlCommand(sSql, conn);
                conn.Open();
                SqlDataReader dr = sqlCmd.ExecuteReader();
                if (dr.HasRows)
                {
                    List<StatusKawin> retval = new List<StatusKawin>();
                    while (dr.Read())
                    {
                        retval.Add(new StatusKawin
                        {
                            Kode = dr["fs_kd_status_kawin_dk"].ToString(),
                            Nama = dr["fs_nm_status_kawin_dk"].ToString()
                        });
                    }
                    return retval;
                }
                return null;
            }

        }

        public void OverwriteConnectionString(string connStr)
        {
            throw new NotImplementedException();
        }

        public void Update(StatusKawin entity)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                string sSql = @"UPDATE   ta_status_kawin_dk
                                SET      fs_kd_status_kawin_dk = @Kode, fs_nm_status_kawin_dk = @Nama
                                WHERE    fs_kd_status_kawin_dk = @Kode";
                SqlCommand sqlCmd = new SqlCommand(sSql, conn);
                sqlCmd.Parameters.Add(new SqlParameter("@Kode", entity.Kode));
                sqlCmd.Parameters.Add(new SqlParameter("@Nama", entity.Nama));
                conn.Open();
                sqlCmd.ExecuteNonQuery();
            }
        }
    }
}
