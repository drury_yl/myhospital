﻿using Ics.Helper7.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHospital.Billing.DataSosial.Ci
{
    public interface INegaraDal : IMasterDataDal<Negara>
    {

    }

    public interface INegaraBl : IMasterDataBl<Negara>
    {

    }
}
