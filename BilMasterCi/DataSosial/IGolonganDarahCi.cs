﻿using Ics.Helper7.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyHospital.Billing.DataSosial;
namespace MyHospital.Billing.DataSosial.Ci 
{
    public interface IGolonganDarahDal: IMasterDataDal<GolonganDarah>
    {
    }
    public interface IGolonganDarahBL : IMasterDataBl<GolonganDarah>
    {
    }
}
