﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial.Ci
{

    public interface ISukuDal : IMasterDataDal<Suku>
    {

    }

    public interface ISukuBl : IMasterDataBl<Suku>
    {

    }

}
