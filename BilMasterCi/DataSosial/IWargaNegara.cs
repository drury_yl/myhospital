﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial;

namespace MyHospital.Billing.DataSosial.Ci
{
    public interface IWargaNegaraDal:IMasterDataDal<WargaNegara>
    {

    }

    public interface IWargaNegaraBl:IMasterDataBl<WargaNegara>
    {

    }
}
