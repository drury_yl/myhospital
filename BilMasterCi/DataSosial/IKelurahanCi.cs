﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass; 

namespace MyHospital.Billing.DataSosial
{
    public interface IKelurahanDal : IMasterDataDal<Kelurahan> 
    {
    }

    public interface IKelurahanBl : IMasterDataBl<Kelurahan>
    {    
    }
}
