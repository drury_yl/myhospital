﻿using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHospital.Billing.DataSosial.Ci
{
    public interface IKecamatanDal : IMasterDataDal<Kecamatan>
    {

    }

    public interface IKecamatanBl : IMasterDataBl<Kecamatan>
    {

    }
}
