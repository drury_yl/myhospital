﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial;

namespace MyHospital.Billing.DataSosial.Ci
{
    public interface IStatusKawinDal : IMasterDataDal<StatusKawin>
    {

    }
    public interface IStatusKawinBl : IMasterDataBl<StatusKawin>
    {

    }
}