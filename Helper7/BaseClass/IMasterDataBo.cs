﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ics.Helper7.BaseClass
{
    public interface IMasterDataBo
    {
        string Kode { get; set; }

        string Nama { get; set; }
    }
}
