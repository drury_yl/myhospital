﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ics.Helper7.BaseClass
{
    public interface IMasterDataBl<T>
    {
        MethodRetVal Save(T entity);

        MethodRetVal  Delete(T entity);

        T GetById(string id);

        List<T> ListAll();

    }
}
