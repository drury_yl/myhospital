﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ics.Helper7.BaseClass
{
    public interface IMasterDataDal<T>
    {
        void Insert(T entity);

        void Update(T entity);

        void Delete(T entity);

        T GetById(string id);

        List<T> ListAll();

        void OverwriteConnectionString(string connStr);
    }
}
