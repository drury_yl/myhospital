﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ics.Helper7.BaseClass
{
    public class MethodRetVal
    {
        public string SourceName { get; set; }

        public string Msg
        {
            get
            {
                string retVal = "";
                foreach (MethodRetValItem item in Items)
                {
                    retVal = retVal + 
                        "ErrorId : " + item.ErrId + 
                       " Message : " + item.Msg + " - ";
                }
                return retVal;
            }
        }

        public List<MethodRetValItem> Items { get; set; }

        public MethodRetVal()
        {
            Items = new List<MethodRetValItem>();
        }
    }

    public class MethodRetValItem
    {
        public string ErrId { get; set; }

        public string Msg { get; set; }
    }
}
