﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using MyHospital.Billing.DataSosial;
using MyHospital.Billing.DataSosial.Bl;
using Ics.Helper7.BaseClass;

namespace MyHospitalServices.Areas.BillingDataSosial.Controllers
{
    public class AgamaController : ApiController
    {
        AgamaBl blAgama = new AgamaBl();

        // GET: api/Agama
        [HttpGet]
        public IEnumerable<Agama> ListAll()
        {
            return blAgama.ListAll();
        }

        // GET: api/Agama/5
        [HttpGet]
        public Agama GetById(string id)
        {
            return blAgama.GetById(id);
        }

        // POST: api/Agama
        [HttpPost]
        public void Post(Agama value)
        {
            MethodRetVal retvaldummy;
            retvaldummy = blAgama.Save(value);
        }

        // DELETE: api/Agama/5
        [HttpDelete]
        public void Delete(string id)
        {
            MethodRetVal retvaldummy;
            Agama agama = new Agama { Kode = id };
            retvaldummy = blAgama.Delete(agama);
        }
    }
}
