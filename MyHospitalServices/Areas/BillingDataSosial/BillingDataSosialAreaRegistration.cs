﻿using System.Web.Mvc;
using System.Web.Http;

namespace MyHospitalServices.Areas.BillingDataSosial
{
    public class BillingDataSosialAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BillingDataSosial";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapHttpRoute(
                "BillingDataSosial_default",
                "BillingDataSosial/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}