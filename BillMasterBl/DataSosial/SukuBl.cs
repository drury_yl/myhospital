﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyHospital.Billing.DataSosial.Ci;
using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial.Bl
{

    public class SukuBl : ISukuBl
    {
        /* Dibuat agar DAL bisa di-inject untuk kepentingan
         * Unit Test (Di-inject dengan Mocking Object */
        private ISukuDal dalSuku;
        public ISukuDal DalSuku
        {
            get { return dalSuku; }
            set { dalSuku = value; }
        }

        public SukuBl()
        {
            dalSuku = new SukuDal();
        }

        public MethodRetVal Save(Suku entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "SukuBl.Save()" };

            #region BR-01: Kode Suku tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE PROPINSI kosong"
                });
            }
            #endregion

            #region BR-01: Nama Suku tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "NAMA PROPINSI kosong"
                });
            }
            #endregion

            #region Data Valid, proses save
            {
                if (dalSuku.GetById(entity.Kode) == null)
                {
                    dalSuku.Insert(entity);
                }
                else dalSuku.Update(entity);
            }
            #endregion

            return retVal;
        }

        public MethodRetVal Delete(Suku entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "SukuBl.Delete()" };
            dalSuku.Delete(entity);
            return retVal;
        }

        public Suku GetById(string id)
        {
            return dalSuku.GetById(id);
        }

        public List<Suku> ListAll()
        {
            return dalSuku.ListAll();
        }
    }
}
