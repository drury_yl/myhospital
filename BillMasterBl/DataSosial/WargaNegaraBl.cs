﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyHospital.Billing.DataSosial.Ci;
using MyHospital.Billing.DataSosial.Dal;
using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class WargaNegaraBl : IWargaNegaraBl
    {
        private IWargaNegaraDal dalWargaNegara;
        public IWargaNegaraDal DalWargaNegara
        {
            get{ return dalWargaNegara; }
            set { dalWargaNegara = value; }
        }

        public WargaNegaraBl()
        {
            dalWargaNegara = new WargaNegaraDal();
        }

        public MethodRetVal Save(WargaNegara entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "WargaNegaraBl.Save()" };

            #region BR-01: Kode WargaNegara tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE WargaNegara kosong"
                });
            }
            #endregion

            #region BR-01: Nama WargaNegara tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "NAMA WargaNegara kosong"
                });
            }
            #endregion

            #region Data Valid, proses save
            if (retVal.Items.Count == 0)
            {
                if (dalWargaNegara.GetById(entity.Kode) == null)
                {
                    dalWargaNegara.Insert(entity);
                }
                else dalWargaNegara.Update(entity);
            }
            #endregion

            return retVal;
        }

        public MethodRetVal Delete(WargaNegara entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "WargaNegaraBl.Delete()" };

            dalWargaNegara.Delete(entity);
            return retVal;
        }

        public WargaNegara GetById(string id)
        {
            return dalWargaNegara.GetById(id);
        }

        public List<WargaNegara> ListAll()
        {
            return dalWargaNegara.ListAll();
        }


    }
}
