﻿using MyHospital.Billing.DataSosial.Ci;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial.Dal;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class NegaraBl : INegaraBl
    {
        private INegaraDal dalNegara;
        public INegaraDal DalNegara
        {
            get { return dalNegara; }
            set { dalNegara = value; }
        }

        public NegaraBl()
        {
            dalNegara = new NegaraDal();
        }

        public MethodRetVal Save(Negara entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "NegaraBl.Save()" };
            #region BR-01: Kode Negara tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE NEGARA kosong"
                });
            }
            #endregion

            #region BR-02: Nama Negara tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "NAMA NEGARA kosong"
                });
            }
            #endregion

            #region Data valid, proses save
            if (retVal.Items.Count() == 0)
            {
                if (dalNegara.GetById(entity.Kode) == null)
                {
                    dalNegara.Insert(entity);
                }
                else
                {
                    dalNegara.Update(entity);
                }
            }
            #endregion
            return retVal;
        }

        public MethodRetVal Delete(Negara entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "NegaraBl.Delete()" };

            dalNegara.Delete(entity);
            return retVal;
        }

        public Negara GetById(string id)
        {
            return dalNegara.GetById(id);
        }

        public List<Negara> ListAll()
        {
            return dalNegara.ListAll();
        }

    }
}
