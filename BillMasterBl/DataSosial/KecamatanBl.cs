﻿using MyHospital.Billing.DataSosial.Ci;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial.Dal;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class KecamatanBl : IKecamatanBl
    {
        /* Dibuat agar DAL bisa di-inject untuk kepentingan
         * Unit Test (Di-inject dengan Mocking Object */
        private IKecamatanDal dalKecamatan;
        public IKecamatanDal DalKecamatan
        {
            get { return dalKecamatan; }
            set { dalKecamatan = value; }
            
        }

        public KecamatanBl()
        {
            dalKecamatan = new KecamatanDal();   
        }

        public MethodRetVal Delete(Kecamatan entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "KecamatanB.Delete()" };

            //TODO Cek apakah ada kelurahan yang memakai Kode Kecamatan ini

            dalKecamatan.Delete(entity);
            return retVal;
        }

        public Kecamatan GetById(string id)
        {
            return dalKecamatan.GetById(id);
        }

        public List<Kecamatan> ListAll()
        {
            return dalKecamatan.ListAll();
        }

        public MethodRetVal Save(Kecamatan entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "KecamatanBl.Save()" };

            #region BR-01 : Kode Kecamatan tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE Kecamatan kosong"
                });
            }
            #endregion

            #region BR-02 : Nama Kecamatan tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg="Nama Kecamatan kosong"
                });
            }
            #endregion

            #region Data Valid, masuk proses save
            if (retVal.Items.Count == 0)
            {
                {
                    if (dalKecamatan.GetById(entity.Kode) == null)
                    {
                        dalKecamatan.Insert(entity);
                    }
                    else dalKecamatan.Update(entity);
                }
            }
            #endregion

            return retVal;
        }
    }
}
