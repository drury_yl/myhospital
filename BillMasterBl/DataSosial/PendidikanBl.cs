﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial.Ci;
using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class PendidikanBl : IPendidikanBl
    {
        /* Dibuat agar DAL bisa di-inject untuk kepentingan
         * Unit Test (di-inject dengan mocking object */
        private IPendidikanDal dalPendidikan;
        public IPendidikanDal DalPendidikan
        {
            get { return dalPendidikan; }
            set { dalPendidikan = value; }
        }
        public PendidikanBl()
        {
            dalPendidikan = new PendidikanDal();
        }


        public MethodRetVal Save(Pendidikan entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "PendidikanBl.Save()" };
            #region BR-01: Kode Pendidikan tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "Kode Pendidikan Kosong"
                });
            }
            #endregion

            #region BR-02: Nama Pendidikan tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "Nama Pendidikan kosong"
                });
            }
            #endregion
            #region Data valid,  proses save
            {
                if (dalPendidikan.GetById(entity.Kode) == null)
                {
                    DalPendidikan.Insert(entity);
                }
                else DalPendidikan.Update(entity);
            }
            #endregion

            return retVal;
        }

        
        public MethodRetVal Delete(Pendidikan entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "PendidikanBl.Delete()" };
            dalPendidikan.Delete(entity);
            return retVal;
        }

        public Pendidikan GetById(string id)
        {
            return dalPendidikan.GetById(id);
        }

        public List<Pendidikan> ListAll()
        {
            return dalPendidikan.ListAll();
        }

        
    }
}
