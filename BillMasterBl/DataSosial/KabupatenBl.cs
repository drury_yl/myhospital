﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial.Ci;
using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial.Bl
{

    public class KabupatenBl : IKabupatenBl
    {
        /* Dibuat agar DAL bisa di-inject untuk kepentingan
         * Unit Test (Di-inject dengan Mocking Object */
        private IKabupatenDal dalKabupaten;
        private IPropinsiDal dalPropinsi;

        public IKabupatenDal DalKabupaten
        {
            get { return dalKabupaten; }
            set { dalKabupaten = value; }
        }

        public IPropinsiDal DalPropinsi
        {
            get { return dalPropinsi; }
            set { dalPropinsi = value; }
        }

        /*  secara default buat instance dari real DAL-nya
            (saat test bisa di-inject lewat property ybs) */
        public KabupatenBl()
        {
            dalKabupaten = new KabupatenDal();
            dalPropinsi = new PropinsiDal();
        }

        public MethodRetVal Save(Kabupaten entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "KabupatenBl.Save()" };

            #region BR-01: Kode Kabupaten tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE Kabupaten kosong"
                });
            }
            #endregion

            #region BR-02: Nama Kabupaten tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "NAMA Kabupaten kosong"
                });
            }
            #endregion

            #region BR-03 : Propinsi harus exist
            if (dalPropinsi.GetById(entity.Propinsi.Kode) == null)
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-03",
                    Msg = "Propinsi tidak valid"
                });
            }
            #endregion


            //TODO : Problem >> Cek apakah Propinsi-nya NULL
            #region Data Valid, proses save Kabupaten
            {
                if (dalKabupaten.GetById(entity.Kode) == null)
                {
                    dalKabupaten.Insert(entity);
                }
                else dalKabupaten.Update(entity);
            }
            #endregion

            return retVal;
        }

        public MethodRetVal Delete(Kabupaten entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "KabupatenBl.Delete()" };
            dalKabupaten.Delete(entity);
            return retVal;
        }

        public Kabupaten GetById(string id)
        {
            return dalKabupaten.GetById(id);
        }

        public List<Kabupaten> ListAll()
        {
            return dalKabupaten.ListAll();
        }
    }
}