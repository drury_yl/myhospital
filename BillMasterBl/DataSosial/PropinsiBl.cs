﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial.Ci;
using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class PropinsiBl : IPropinsiBl
    {
        /* Dibuat agar DAL bisa di-inject untuk kepentingan
         * Unit Test (Di-inject dengan Mocking Object */
        private IPropinsiDal dalPropinsi;
        public IPropinsiDal DalPropinsi
        {
            get { return dalPropinsi; }
            set { dalPropinsi = value;}
        }

        public PropinsiBl()
        {
            dalPropinsi = new PropinsiDal();
        }

        public MethodRetVal Save(Propinsi entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "PropinsiBl.Save()" };

            #region BR-01: Kode Propinsi tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE PROPINSI kosong"
                });
            }
            #endregion

            #region BR-01: Nama Propinsi tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "NAMA PROPINSI kosong"
                });
            }
            #endregion

            #region Data Valid, proses save
            {
                if (dalPropinsi.GetById(entity.Kode) == null)
                {
                    dalPropinsi.Insert(entity);
                }
                else dalPropinsi.Update(entity);
            }
            #endregion

            return retVal;
        }

        public MethodRetVal Delete(Propinsi entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "PropinsiBl.Delete()" };

            //TODO Cek apakah ada kabupaten yang memakai Kode Propinsi ini

            dalPropinsi.Delete(entity);
            return retVal;
        }

        public Propinsi GetById(string id)
        {
            return dalPropinsi.GetById(id);
        }

        public List<Propinsi> ListAll()
        {
            return dalPropinsi.ListAll();
        }
    }

 }

