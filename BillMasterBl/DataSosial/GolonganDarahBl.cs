﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial.Ci;
using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class GolonganDarahBl : IGolonganDarahBL
    {
        private IGolonganDarahDal dalGolonganDarah;

        public IGolonganDarahDal DalGolonganDarah
        {
            get { return dalGolonganDarah; }
            set { dalGolonganDarah= value; } 
        }
        public GolonganDarahBl()
        {
            dalGolonganDarah = new GolonganDarahDal();
        }

        public MethodRetVal Save(GolonganDarah entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "GolonganDarahBl.Save()" };
            #region BR-01 Kode Golongan Darah Tidak Boleh Kosong 
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE GOLONGAN DARAH kosong"
                });
            }
            #endregion 

            #region BR-02 Nama Golongan Darah Tidak Boleh Kosong 
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "NAMA GOLONGAN DARAH kosong"
                });
            }
            #endregion
            #region data valid save golongan darah 
            {
                if (dalGolonganDarah.GetById(entity.Kode) == null)
                {
                    dalGolonganDarah.Insert(entity);
                }
                else dalGolonganDarah.Update(entity);
                
            }
            #endregion
            return retVal;
        }

        public MethodRetVal Delete(GolonganDarah entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "GolonganDarahBl.Delete" };
            dalGolonganDarah.Delete(entity);
            return retVal;
        }

        public GolonganDarah GetById(string id)
        {
            return dalGolonganDarah.GetById(id);
        }

        public List<GolonganDarah> ListAll()
        {
            return dalGolonganDarah.ListAll();
        }

    }
}
