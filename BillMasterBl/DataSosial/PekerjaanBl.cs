﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial.Dal;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class PekerjaanBl : IPekerjaanBL
    {
        private IPekerjaanDAL dalPekerjaan;
        public IPekerjaanDAL DalPekerjaan
        {
            get { return dalPekerjaan; }
            set { dalPekerjaan = value; }
        }

        public PekerjaanBl()
        {
            dalPekerjaan = new PekerjaanDal();
        }

        public MethodRetVal Delete(Pekerjaan entity)
        {
            MethodRetVal RetVal = new MethodRetVal { SourceName = "PekerjaanBl.Delete()" };
            dalPekerjaan.Delete(entity);
            return RetVal;
        }

        public Pekerjaan GetById(string id)
        {
            return dalPekerjaan.GetById(id);
        }

        public List<Pekerjaan> ListAll()
        {
            return dalPekerjaan.ListAll();
        }

        public MethodRetVal Save(Pekerjaan entity)
        {
            MethodRetVal RetVal = new MethodRetVal { SourceName = "PekerjaanBl.Save()" };

            #region BR-01: Kode Pekerjaan tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                RetVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "Kode Pekerjaan kosong"
                });
            }
            #endregion

            #region BR-02: Nama Pekerjaan tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                RetVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "Nama Pekerjaan kosong"
                });
            }
            #endregion

            #region SAVE
            {
                if (dalPekerjaan.GetById(entity.Kode) == null)
                {
                    dalPekerjaan.Insert(entity);
                }
                else dalPekerjaan.Update(entity);
            }
            #endregion

            return RetVal;
        }
    }
}
