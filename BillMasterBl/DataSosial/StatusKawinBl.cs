﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;
using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial.Ci;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class StatusKawinBl : IStatusKawinBl
    {
        private IStatusKawinDal dalStatusKawin;

        public IStatusKawinDal DalStatusKawin
        {
            get { return dalStatusKawin; }
            set { dalStatusKawin = value; }
        }

        public StatusKawinBl()
        {
            dalStatusKawin = new StatusKawinDal();
        }

        public MethodRetVal Delete(StatusKawin entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "StatusKawinBl.Save()" };
            dalStatusKawin.Delete(entity);
            return retVal;
        }

        public StatusKawin GetById(string id)
        {
            return dalStatusKawin.GetById(id);
        }

        public List<StatusKawin> ListAll()
        {
            return dalStatusKawin.ListAll();
        }

        public MethodRetVal Save(StatusKawin entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "StatusKawinBl.Save()" };
            #region BR-01: Kode Status Kawin tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE STATUS KAWIN kosong"
                });
            }
            #endregion

            #region BR-01: Nama Status Kawin tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "NAMA STATUS KAWIN kosong"
                });
            }
            #endregion

            #region Data Valid, proses save
            if(retVal.Items.Count==0)
            {
                if (dalStatusKawin.GetById(entity.Kode) == null)
                {
                    dalStatusKawin.Insert(entity);
                }
                else
                {
                    dalStatusKawin.Update(entity);
                }
            }
            #endregion
            return retVal;
        }
    }
}
