﻿using MyHospital.Billing.DataSosial.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial
{

    public class AgamaBl : IAgamaBl
    {
        /* Dibuat agar DAL bisa di-inject untuk kepentingan
         * Unit Test (Di-inject dengan Mocking Object */
        private IAgamaDal dalAgama;
        public IAgamaDal DalAgama
        {
            get { return dalAgama; }
            set { dalAgama = value; }
        }

        /*  secara default buat instance dari real DAL-nya
            (saat test bisa di-inject lewat property ybs) */
        public AgamaBl()
        {
            dalAgama = new AgamaDal();
        }

        public MethodRetVal Save(Agama entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "AgamaBl.Save()" };

            #region BR-01: Kode Agama tidak boleh kosong
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "KODE Agama kosong"
                });
            }
            #endregion

            #region BR-02: Nama Agama tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "NAMA Agama kosong"
                });
            }
            #endregion

            #region Data Valid, proses save
            {
                if (dalAgama.GetById(entity.Kode) == null)
                {
                    dalAgama.Insert(entity);
                }
                else dalAgama.Update(entity);
            }
            #endregion

            return retVal;
        }

        public MethodRetVal Delete(Agama entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "AgamaBl.Delete()" };
            dalAgama.Delete(entity);
            return retVal;
        }

        public Agama GetById(string id)
        {
            return dalAgama.GetById(id);
        }

        public List<Agama> ListAll()
        {
            return dalAgama.ListAll();
        }
    }
}
