﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyHospital.Billing.DataSosial.Dal;
using MyHospital.Billing.DataSosial.Ci;
using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial.Bl
{
    public class KelurahanBl : IKelurahanBl
    {
        private IKelurahanDal dalKelurahan;
        public IKelurahanDal DalKelurahan
        {
            get { return dalKelurahan;  }
            set { dalKelurahan = value; }       
        }

        public KelurahanBl()
        {
            dalKelurahan = new KelurahanDal();
        }

        public MethodRetVal Delete(Kelurahan entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "KelurahanBl.Delete()" };
            dalKelurahan.Delete(entity);
            return retVal;
        }

        public Kelurahan GetById(string id)
        {
            return dalKelurahan.GetById(id);
        }

        public List<Kelurahan> ListAll()
        {
            return dalKelurahan.ListAll();
        }

        public MethodRetVal Save(Kelurahan entity)
        {
            MethodRetVal retVal = new MethodRetVal { SourceName = "KelurahanBl.Save()" };

            #region BR-01: Kode Kelurahan tidak boleh kosong            
            if (entity.Kode.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-01",
                    Msg = "Kode Kelurahan Kosong"
                });
            }
            #endregion

            #region BR-02: Nama Kelurahan tidak boleh kosong
            if (entity.Nama.Trim() == "")
            {
                retVal.Items.Add(new MethodRetValItem
                {
                    ErrId = "BR-02",
                    Msg = "Nama Kelurahan Kosong"
                });
            }
            #endregion

            #region Data valid, Proses Save
            if (retVal.Items.Count == 0)
            {
                {
                    if (dalKelurahan.GetById(entity.Kode) == null)
                    {
                        dalKelurahan.Insert(entity);
                    }
                    else
                        dalKelurahan.Update(entity);
                }
            }            
            #endregion
            return retVal;
        }
    }
}
