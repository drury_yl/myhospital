﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ics.Helper7.BaseClass;

namespace MyHospital.Billing.DataSosial
{
    public class Agama : IMasterDataBo
    {
        public string Kode { get; set; }

        public string Nama { get; set; }
    }
}
