﻿using Ics.Helper7.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHospital.Billing.DataSosial
{
    public class Pekerjaan : IMasterDataBo
    {
        public string Kode { get; set; }

        public string Nama { get; set; }
    }
}
